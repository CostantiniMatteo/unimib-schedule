package com.unimib_schedule;


import android.app.Activity;
import android.app.Instrumentation;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.core.internal.deps.guava.base.Predicate;
import androidx.test.espresso.core.internal.deps.guava.collect.Iterables;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.espresso.matcher.RootMatchers;
import androidx.test.espresso.util.TreeIterables;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import androidx.test.runner.lifecycle.Stage;

import com.unimib_schedule.search_schedule.SearchCourseActivity;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;
import com.unimib_schedule.edit_schedule.EditScheduleRecyclerViewAdapter;
import com.unimib_schedule.home.HomeActivity;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import static android.service.autofill.Validators.and;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.DrawerMatchers.isClosed;
import static androidx.test.espresso.core.internal.deps.guava.base.Preconditions.checkNotNull;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static junit.framework.TestCase.fail;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
@LargeTest
@FixMethodOrder(MethodSorters.JVM)
public class SearchScheduleTest {

    private final Instrumentation instrumentation = getInstrumentation();

    @Rule
    public ActivityTestRule<HomeActivity> homeActivityRule = new ActivityTestRule<>(HomeActivity.class);

    @Rule
    public ActivityTestRule<SearchCourseActivity> searchCourseActivityRule = new ActivityTestRule<>(SearchCourseActivity.class);

    @Test
    public void SaveComputerScienceSchedule() throws Throwable {
        // Ricerca per corso di laurea e salvataggio
        searchCoursesByDegreeCourse("Informatica",
                "Secondo Semestre",
                "Informatica (Magistrali)",
                "1"
        );
        saveCourse(new ArrayList<>(Arrays.asList("Sistemi informativi", "Bioinformatica", "Metodi del calcolo scientifico")));
        deleteCourse();
    }

    @Test
    public void SaveMarineSciencesSchedule() throws Throwable{
        // Ricerca per corso di laurea e salvataggio
        searchCoursesByDegreeCourse("Marine Sciences",
                "Secondo Semestre",
                "Marine sciences - scienze marine (Magistrali)",
                "2"
        );
        saveCourse(new ArrayList<>(Arrays.asList("Food law and policy")));
        deleteCourse();
    }

    @Test
    public void SaveProfessorSchedule() throws Throwable {
        // Ricerca orario professore e salvataggio
        searchCoursesByProfessor("Domenico Giorgio Sorrenti");
        saveCourse(new ArrayList<>(Arrays.asList("Robotica e automazione")));
        deleteCourse();
    }

    @Test
    public void SaveSecondaryProfessorSchedule() throws Throwable {
        // Ricerca orario professore secondario e salvataggio
        searchCoursesByProfessor("Giuseppe Vacca");
        saveCourse(new ArrayList<>(Arrays.asList("Metodi del calcolo scientifico")));
        deleteCourse();
    }

    @Test
    public void SaveTeachingAssistantSchedule() throws Throwable {
        // Ricerca orario esercitatore e salvataggio
        searchCoursesByProfessor("Luca Crociani");
        saveCourse(new ArrayList<>(Arrays.asList("Sistemi complessi: modelli e simulazione")));
        deleteCourse();
    }


    @Test
    public void SearchWithInvalidFaculty(){
        searchCoursesByDegreeCourseWithInvalidFaculty();
    }

    @Test
    public void SearchWithInvalidDidacticPeriod(){
        searchCoursesByDegreeCourseWithInvalidDidacticPeriod("Economia");
    }

    @Test
    public void SearchWithInvalidDegreeCourse() throws Throwable {
        searchCoursesByDegreeCourseWithInvalidDegreeCourse("Fisica", "Primo Semestre");
    }

    @Test
    public void SearchWithInvalidYear() throws Throwable {
        searchCoursesByDegreeCourseWithInvalidYear("Geologia", "Primo Semestre", "Scienze e tecnologie geologiche (Triennali)");
    }

    @Test
    public void SearchWithInvalidProfessor(){
        searchCoursesByProfessorWithInvalidProfessor("Mario Rossi");
    }

    // TODO: non funziona bisogna trovare il modo di capire se esce il suggerimento oppure no
    private void searchCoursesByProfessorWithInvalidProfessor(String professor) {
        // Button di aggiunta, e passaggio ad un'altra activity
        onView(withId(HomeActivity.ADD_FAB_ID)).perform(click());

        // Selezione tab ricerca per professore
        onView(withText("PROFESSORE")).perform(click());

        // Riempimento della textview
        onView(withId(R.id.autocomplete_professor)).perform(typeText(professor), closeSoftKeyboard());
        try {
            onView(withText(professor))
                    .inRoot(RootMatchers.isPlatformPopup()).check(matches(isDisplayed()));
            fail();
        } catch(Exception e){

        }
        //onView(allOf(withId(R.id.text1), withText(professor))).check(matches(isDisplayed()));

        // Button di ricerca, e passaggio ad activity di visualizzazione
        onView(withId(R.id.searchByProfessor)).perform(click());

    }

    private void searchCoursesByDegreeCourseWithInvalidYear(String faculty, String didacticPeriod, String degreeCourse) throws InterruptedException {
        // Button di aggiunta, e passaggio ad un'altra activity
        onView(withId(HomeActivity.ADD_FAB_ID)).perform(click());

        // Selezione tab ricerca per corso di laurea
        onView(withText("CDL")).perform(click());

        // Spinner Facoltà
        onView(withId(R.id.faculty_spinner)).perform(click());
        onData(allOf(is(instanceOf(Faculty.class)), withMyValue(faculty))).perform(click());

        // Spinner del didactic period
        onData(allOf(is(instanceOf(DidacticPeriod.class)), withMyValue(didacticPeriod))).perform(click());

        // Aspetto finchè la Dialog non si chiude
        onView(withText(R.string.progressDialogTitle)).check(matches(isDisplayed()));
        Thread.sleep(10000);

        // Spinner per corso di laurea
        onData(allOf(is(instanceOf(DegreeCourse.class)), withMyValue(degreeCourse))).perform(click());

        // Spinner anno di corso
        onData(allOf(is(instanceOf(String.class)), withMyValue("—"))).perform(click());

        // Button di ricerca non cliccabile
        onView(withId(R.id.searchByCourse)).check(matches(not(isClickable())));
    }

    private void searchCoursesByDegreeCourseWithInvalidDegreeCourse(String faculty, String didactic_period) throws InterruptedException {
        // Button di aggiunta, e passaggio ad un'altra activity
        onView(withId(HomeActivity.ADD_FAB_ID)).perform(click());

        // Selezione tab ricerca per corso di laurea
        onView(withText("CDL")).perform(click());

        // Spinner Facoltà
        onView(withId(R.id.faculty_spinner)).perform(click());
        onData(allOf(is(instanceOf(Faculty.class)), withMyValue(faculty))).perform(click());

        // Spinner Didactic Period
        onData(allOf(is(instanceOf(DidacticPeriod.class)), withMyValue(didactic_period))).perform(click());
        Thread.sleep(10000);

        // Spinner per corso di laurea
        onData(allOf(is(instanceOf(DegreeCourse.class)), withMyValue("—"))).perform(click());

        // Button di ricerca non cliccabile
        onView(withId(R.id.searchByCourse)).check(matches(not(isClickable())));

    }

    @After
    public void goHome(){
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT))) // Left Drawer should be closed.
                .perform(DrawerActions.open());

        onView(withId(R.id.left_drawer))
                .perform(NavigationViewActions.navigateTo(R.id.home));
    }

    private void searchCoursesByDegreeCourseWithInvalidDidacticPeriod(String faculty) {
        // Button di aggiunta, e passaggio ad un'altra activity
        onView(withId(HomeActivity.ADD_FAB_ID)).perform(click());

        // Selezione tab ricerca per corso di laurea
        onView(withText("CDL")).perform(click());

        // Spinner Facoltà
        onView(withId(R.id.faculty_spinner)).perform(click());
        onData(allOf(is(instanceOf(Faculty.class)), withMyValue(faculty))).perform(click());

        // Spinner Didactic Period
        onData(allOf(is(instanceOf(DidacticPeriod.class)), withMyValue("—"))).perform(click());

        // Button di ricerca non cliccabile
        onView(withId(R.id.searchByCourse)).check(matches(not(isClickable())));

    }

    private void searchCoursesByDegreeCourseWithInvalidFaculty() {
        // Button di aggiunta, e passaggio ad un'altra activity
        onView(withId(HomeActivity.ADD_FAB_ID)).perform(click());

        // Selezione tab ricerca per corso di laurea
        onView(withText("CDL")).perform(click());

        // Spinner Facoltà
        onView(withId(R.id.faculty_spinner)).perform(click());
        onData(allOf(is(instanceOf(Faculty.class)), withMyValue("—"))).perform(click());

        // Button di ricerca non cliccabile
        onView(withId(R.id.searchByCourse)).check(matches(not(isClickable())));
    }

    private void searchCoursesByProfessor(String professor) throws InterruptedException {
        // Button di aggiunta, e passaggio ad un'altra activity
        onView(withId(HomeActivity.ADD_FAB_ID)).perform(click());

        // Selezione tab ricerca per professore
        onView(withText("PROFESSORE")).perform(click());

        // Riempimento della textview
        onView(withId(R.id.autocomplete_professor)).perform(typeText(professor), closeSoftKeyboard());

        onView(withText(professor)).inRoot(isPlatformPopup()).perform(click());

        // Button di ricerca, e passaggio ad activity di visualizzazione
        onView(withId(R.id.searchByProfessor)).perform(click());

    }

    private void deleteCourse() {
        // Button cancellazione
        onView(withId(R.id.action_delete)).perform(click());

        // OK per dialog di avviso
        onView(withText(android.R.string.yes))
                .inRoot(isDialog())
                .check(matches(isDisplayed()))
                .perform(click());
    }

    public void searchCoursesByDegreeCourse(String faculty, String didacticPeriod, String degreeCourse, String year) throws Throwable {
        // Button di aggiunta, e passaggio ad un'altra activity
        onView(withId(HomeActivity.ADD_FAB_ID)).perform(click());

        // Selezione tab ricerca per corso di laurea
        onView(withText("CDL")).perform(click());

        // Spinner Facoltà
        onView(withId(R.id.faculty_spinner)).perform(click());
        onData(allOf(is(instanceOf(Faculty.class)), withMyValue(faculty))).perform(click());

        // Spinner del didactic period
        onData(allOf(is(instanceOf(DidacticPeriod.class)), withMyValue(didacticPeriod))).perform(click());

        // Aspetto finchè la Dialog non si chiude
        onView(withText(R.string.progressDialogTitle)).check(matches(isDisplayed()));
        Thread.sleep(7000);

        // Spinner per corso di laurea
        onData(allOf(is(instanceOf(DegreeCourse.class)), withMyValue(degreeCourse))).perform(click());

        // Spinner anno di corso
        onData(allOf(is(instanceOf(String.class)), withMyValue(year))).perform(click());

        // Button di ricerca, e passaggio ad activity di visualizzazione
        onView(withId(R.id.searchByCourse)).perform(click());

    }

    public void saveCourse(ArrayList<String> courses) throws Throwable{

        // Button di salvataggio nella activity di visualizzazione
        onView(withId(R.id.saveFab)).perform(click());

        // OK per dialog di avviso
        onView(withText(android.R.string.yes))
                .inRoot(isDialog())
                .check(matches(isDisplayed()))
                .perform(click());

        /*onView(withId(R.id.recycler_view))
                .check(matches(atPosition(0, withText("Sistemi informativi"))));*/

        RecyclerView recyclerView = getCurrentActivity().findViewById(R.id.recycler_view);
        EditScheduleRecyclerViewAdapter adapter = (EditScheduleRecyclerViewAdapter) recyclerView.getAdapter();
        int itemCount = adapter.getItemCount();

        for(int i = 0; i < itemCount; i++){
            if(!courses.contains(adapter.getItem(i).name)){
                onView(withId(R.id.recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(i, click()));
            }
        }

        // Save button e ritorno nella home
        onView(withId(R.id.saveFab)).perform(click());

    }

    Activity getCurrentActivity() throws Throwable {
        final Activity[] currentActivity = {null};

        getInstrumentation().runOnMainSync(new Runnable(){
            public void run(){
                Collection<Activity> resumedActivity = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
                Iterator<Activity> it = resumedActivity.iterator();
                currentActivity[0] = it.next();
            }
        });

        return currentActivity[0];
    }

    public static Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        checkNotNull(itemMatcher);
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                EditScheduleRecyclerViewAdapter.ViewHolder viewHolder = (EditScheduleRecyclerViewAdapter.ViewHolder)view.findViewHolderForAdapterPosition(position);
                if (viewHolder == null) {
                    // has no item on such position
                    return false;
                }
                return itemMatcher.matches(viewHolder.mNameView);
            }
        };
    }

    public static <T> Matcher<T> withMyValue(final String name) {
        return new BaseMatcher<T>() {
            @Override
            public boolean matches(Object item) {
                return item.toString().equals(name);
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }




}
