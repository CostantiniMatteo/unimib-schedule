package com.unimib_schedule.search_schedule;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.unimib_schedule.R;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.Professor;
import com.unimib_schedule.display_lectures.DisplayLecturesActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchScheduleByProfessorFragment extends Fragment {
    private static Comparator alphabeticComparator = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            if (o1 != null && o2 != null) {
                return o1.toString().compareTo(o2.toString());
            }
            return 0;
        }
    };

    private SearchScheduleByProfessorViewModel viewModel;
    private ArrayAdapter<Professor> professorAdapter;

    private AutoCompleteTextView professorTextView;
    private Observer<List<Professor>> professorObserver;
    private LiveData<List<Professor>> professorLiveData;
    private Observer<List<Course>> courseObserver;
    private LiveData<List<Course>> courseLiveData;

    private Professor selectedProfessor;
    private List<Course> retrievedCourses;
    private Button searchButton;

    public static SearchScheduleByProfessorFragment newInstance() {
        return new SearchScheduleByProfessorFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_schedule_by_professor, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        searchButton = view.findViewById(R.id.searchByProfessor);
        searchButton.setOnClickListener(getCreateButtonClickListener());
        setSearchButtonEnabled(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        viewModel = ViewModelProviders.of(this).get(SearchScheduleByProfessorViewModel.class);

        professorAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
        professorTextView = getView().findViewById(R.id.autocomplete_professor);
        professorTextView.setThreshold(0);
        professorTextView.setAdapter(professorAdapter);
        professorTextView.setOnItemClickListener(createProfessorClickListener(this));
        professorTextView.addTextChangedListener(createTextChangedListener());

        professorObserver = createProfessorObserver();
        professorLiveData = viewModel.getProfessors();
        professorLiveData.observe(this, professorObserver);
    }

    private TextWatcher createTextChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                setSearchButtonEnabled(false);
            }
        };
    }

    private Observer<List<Professor>> createProfessorObserver() {
        return new Observer<List<Professor>>() {
            @Override
            public void onChanged(List<Professor> professors) {
                ArrayList<Professor> toAdd = new ArrayList<>(professors);
                Collections.sort(toAdd, alphabeticComparator);
                professorAdapter.clear();
                professorAdapter.addAll(toAdd);
                professorAdapter.notifyDataSetChanged();
            }
        };
    }

    private Observer<List<Course>> createCourseObserver() {
        return new Observer<List<Course>>() {
            @Override
            public void onChanged(List<Course> courses) {
                Log.d("AddFragment", "Found " + courses.size() + " course(s)");
                for (Course c : courses) {
                    Log.d("AddFragment", "" + c);
                }
                retrievedCourses = courses;
            }
        };
    }

    @NotNull
    private View.OnClickListener getCreateButtonClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("Button Click", "courses: " + courseLiveData.getValue());

                ArrayList<String> courseIds = new ArrayList<>(retrievedCourses.size());

                for (Course c : retrievedCourses) {
                    courseIds.add(c.id);
                }

                Intent intent = new Intent(getActivity(), DisplayLecturesActivity.class);
                intent.putStringArrayListExtra(DisplayLecturesActivity.COURSE_IDS_KEY, courseIds);
                startActivity(intent);
            }
        };
    }

    private AdapterView.OnItemClickListener createProfessorClickListener(final SearchScheduleByProfessorFragment fragment) {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Professor professor = (Professor) parent.getItemAtPosition(position);
                Log.d("AddByProfessorFragment", "Selected professor: " + professor);

                selectedProfessor = professor;
                courseLiveData = viewModel.getCoursesByProfessor(selectedProfessor);
                courseObserver = createCourseObserver();
                courseLiveData.observe(fragment, courseObserver);

                setSearchButtonEnabled(true);
            }
        };
    }


    private void setSearchButtonEnabled(boolean enabled) {
        searchButton.setEnabled(enabled);
        searchButton.setClickable(enabled);
        if (enabled) {
            searchButton.setAlpha(1);
        } else {
            searchButton.setAlpha(0.5f);
        }
    }

}
