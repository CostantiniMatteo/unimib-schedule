package com.unimib_schedule.search_schedule;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;
import com.unimib_schedule.utils.BaseViewModel;

import java.util.List;

public class SearchScheduleByCourseViewModel extends BaseViewModel {

    public SearchScheduleByCourseViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<Faculty>> getFaculties() {
        return getFaculties(false);
    }

    public LiveData<List<Faculty>> getFaculties(boolean forceRefresh) {
        return repository.getFaculties(forceRefresh);
    }

    public LiveData<List<DegreeCourse>> getDegreeCourses(DidacticPeriod didacticPeriod) {
        return repository.getDegreeCourses(didacticPeriod);
    }

    public LiveData<List<DidacticPeriod>> getDidacticPeriodsByFaculty(Faculty faculty) {
        return repository.getDidacticPeriodsByFaculty(faculty);
    }

    public LiveData<List<String>> getDegreeCourseYears(DidacticPeriod didacticPeriod, DegreeCourse degreeCourse) {
        return repository.getDegreeCourseYears(didacticPeriod, degreeCourse);
    }

    public LiveData<List<Course>> getCoursesByDidacticPeriodAndYear(DidacticPeriod didacticPeriod, DegreeCourse degreeCourse, String year) {
        return repository.findByDidacticPeriodDegreeCourseAndYear(didacticPeriod, degreeCourse, year);
    }

}
