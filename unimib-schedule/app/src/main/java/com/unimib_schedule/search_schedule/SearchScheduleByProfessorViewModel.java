package com.unimib_schedule.search_schedule;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.data.model.Professor;
import com.unimib_schedule.utils.BaseViewModel;

import java.util.List;

public class SearchScheduleByProfessorViewModel extends BaseViewModel {

    public SearchScheduleByProfessorViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<Professor>> getProfessors() {
        return repository.getProfessors();
    }

    public LiveData<List<Course>> getCoursesByProfessor(Professor professor) {
        return repository.findByProfessor(professor);
    }

    public void updatePersonalSchedule(List<PersonalScheduleCourse> personalSchedule) {
        repository.updatePersonalSchedule(personalSchedule);
    }
}
