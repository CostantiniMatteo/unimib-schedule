package com.unimib_schedule.search_schedule;

import android.os.Bundle;

import com.unimib_schedule.R;
import com.unimib_schedule.utils.BaseActivity;

/***
 * Activity that holds the tab view used to search schedules either by Degree Course or Professor
 */
public class SearchCourseActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, SearchScheduleFormFragment.newInstance())
                    .commit();
        }
    }

}
