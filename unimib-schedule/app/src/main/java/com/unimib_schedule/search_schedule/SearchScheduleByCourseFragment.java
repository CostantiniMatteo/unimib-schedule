package com.unimib_schedule.search_schedule;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.unimib_schedule.R;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;
import com.unimib_schedule.display_lectures.DisplayLecturesActivity;
import com.unimib_schedule.utils.FragmentWithProgressDialog;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchScheduleByCourseFragment extends FragmentWithProgressDialog {
    private static Faculty facultyPlaceHolder = new Faculty("", "", "—");
    private static DegreeCourse degreeCoursePlaceholder = new DegreeCourse("", "—", "");
    private static DidacticPeriod didacticPeriodPlaceholder = new DidacticPeriod("", "—", "", null, null, "", "");
    private static String yearPlaceholder = "—";

    private static Comparator alphabeticComparator = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            if (o1 != null && o2 != null) {
                return o1.toString().compareTo(o2.toString());
            }
            return 0;
        }
    };


    private SearchScheduleByCourseViewModel viewModel;
    private ArrayAdapter<DegreeCourse> degreeCourseAdapter;
    private ArrayAdapter<Faculty> facultyAdapter;
    private ArrayAdapter<String> yearAdapter;
    private ArrayAdapter<DidacticPeriod> didactidPeriodAdapter;

    private ProgressDialog progressDialog;
    private Button searchButton;

    private Spinner degreeSpinner;
    private Spinner facultySpinner;
    private Spinner yearSpinner;
    private Spinner didacticPeriodSpinner;
    private Observer<List<Faculty>> facultyObserver;
    private Observer<List<DegreeCourse>> degreeCourseObserver;
    private Observer<List<String>> yearObserver;
    private Observer<List<DidacticPeriod>> didacticPeriodObserver;
    private Observer<List<Course>> courseObserver;

    private LiveData<List<Faculty>> facultyLiveData;
    private LiveData<List<DegreeCourse>> degreeCourseLiveData;
    private LiveData<List<String>> yearLiveData;
    private LiveData<List<DidacticPeriod>> didacticPeriodLiveData;
    private LiveData<List<Course>> courseLiveData;

    private Faculty selectedFaculty;
    private DegreeCourse selectedDegreeCourse;
    private DidacticPeriod selectedDidacticPeriod;
    private String selectedYear;
    private List<Course> retrievedCourses;

    public static SearchScheduleByCourseFragment newInstance() {
        return new SearchScheduleByCourseFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_schedule_by_course, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(SearchScheduleByCourseViewModel.class);

        // settiamo tutti gli Adapter per evitare NullPointerExceptions
        facultyAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        facultyAdapter.add(facultyPlaceHolder);
        degreeCourseAdapter = new ArrayAdapter<>(getActivity(), R.layout.custom_spinner_item);
        degreeCourseAdapter.add(degreeCoursePlaceholder);
        yearAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        yearAdapter.add(yearPlaceholder);
        didactidPeriodAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item);
        didactidPeriodAdapter.add(didacticPeriodPlaceholder);

        facultyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        degreeCourseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        didactidPeriodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        // Le Faculty
        facultySpinner = getView().findViewById(R.id.faculty_spinner);

        facultySpinner.setAdapter(facultyAdapter);
        facultySpinner.setOnItemSelectedListener(createFacultyClickListener(this));

        facultyObserver = createFacultyObserver();
        refreshUniversityData();

        // i DidacticPeriod (semestre nella UI)
        didacticPeriodSpinner = getView().findViewById(R.id.didactic_period_spinner);
        didacticPeriodSpinner.setAdapter(didactidPeriodAdapter);
        didacticPeriodSpinner.setOnItemSelectedListener(createDidacticPeriodClickListener(this));

        // i DegreeCourse (Corsi di Laurea)
        degreeSpinner = getView().findViewById(R.id.degree_course_spinner);
        degreeSpinner.setAdapter(degreeCourseAdapter);
        degreeSpinner.setOnItemSelectedListener(createDegreeCourseClickListener(this));

        // gli anni del corso di laurea
        yearSpinner = getView().findViewById(R.id.year_spinner);
        yearSpinner.setAdapter(yearAdapter);

        yearSpinner.setOnItemSelectedListener(createYearClickListener(this));

        searchButton = view.findViewById(R.id.searchByCourse);
        searchButton.setOnClickListener(createButtonClickListener());
        setSearchButtonEnabled(false);

        clearFromDegreeCourse();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    private Observer<List<Faculty>> createFacultyObserver() {
        return new Observer<List<Faculty>>() {
            @Override
            public void onChanged(List<Faculty> faculties) {
                List<Faculty> toAdd = new ArrayList<>(faculties);
                Collections.sort(toAdd, alphabeticComparator);
                toAdd.add(0, facultyPlaceHolder);
                facultyAdapter.clear();
                facultyAdapter.addAll(toAdd);
                facultyAdapter.notifyDataSetChanged();
            }
        };
    }


    private Observer<List<DegreeCourse>> createDegreeCourseObserver() {
        return new Observer<List<DegreeCourse>>() {
            @Override
            public void onChanged(List<DegreeCourse> degreeCourses) {
                List<DegreeCourse> toAdd = new ArrayList<>(degreeCourses);
                Collections.sort(toAdd, alphabeticComparator);
                toAdd.add(0, degreeCoursePlaceholder);
                degreeCourseAdapter.clear();
                degreeCourseAdapter.addAll(toAdd);
                degreeCourseAdapter.notifyDataSetChanged();
            }
        };
    }

    private Observer<List<String>> createAcademicYearObserver() {
        return new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> years) {
                List<String> toAdd = new ArrayList<>(years);
                Collections.sort(toAdd, alphabeticComparator);
                toAdd.add(0, yearPlaceholder);
                yearAdapter.clear();
                yearAdapter.addAll(toAdd);
                yearAdapter.notifyDataSetChanged();
            }
        };
    }

    private Observer<List<DidacticPeriod>> createDidacticPeriodObserver() {
        return new Observer<List<DidacticPeriod>>() {
            @Override
            public void onChanged(List<DidacticPeriod> didacticPeriods) {
                List<DidacticPeriod> toAdd = new ArrayList<>(didacticPeriods);
                Collections.sort(toAdd, alphabeticComparator);
                toAdd.add(0, didacticPeriodPlaceholder);
                didactidPeriodAdapter.clear();
                didactidPeriodAdapter.addAll(toAdd);
                didactidPeriodAdapter.notifyDataSetChanged();
                Log.d("DidacticPeriodObserver", "New didacticPeriods: " + didacticPeriods.size());
            }
        };
    }

    private Observer<List<Course>> createCourseObserver() {
        return new Observer<List<Course>>() {
            @Override
            public void onChanged(List<Course> courses) {
                Log.d("AddFragment", "Found " + courses.size() + " course(s)");
                for (Course c : courses) {
                    Log.d("AddFragment", "" + c);
                }
                retrievedCourses = courses;
            }
        };
    }

    @NotNull
    private View.OnClickListener createButtonClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("Button Click", "courses: " + courseLiveData.getValue());

                ArrayList<String> courseIds = new ArrayList<>(retrievedCourses.size());

                for (Course c : retrievedCourses) {
                    courseIds.add(c.id);
                }
                Intent intent = new Intent(getActivity(), DisplayLecturesActivity.class);
                intent.putStringArrayListExtra(DisplayLecturesActivity.COURSE_IDS_KEY, courseIds);
                startActivity(intent);
            }
        };
    }

    /*
    Il Listener che ascolta i cambiamenti della TextView delle Faculty
     */
    private AdapterView.OnItemSelectedListener createFacultyClickListener(final SearchScheduleByCourseFragment fragment) {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clearFromDidacticPeriod();
                if (position == 0) {
                    return;
                }
                Faculty faculty = (Faculty) parent.getItemAtPosition(position);
                Log.d("AddFragment", "Selected faculty: " + faculty);

                selectedFaculty = faculty;
                didacticPeriodLiveData = viewModel.getDidacticPeriodsByFaculty(faculty);
                didacticPeriodObserver = createDidacticPeriodObserver();
                didacticPeriodLiveData.observe(fragment, didacticPeriodObserver);
                Log.d("AddFragment", "Live data: " + didacticPeriodLiveData.getValue());

                didacticPeriodSpinner.performClick();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private AdapterView.OnItemSelectedListener createDidacticPeriodClickListener(final SearchScheduleByCourseFragment fragment) {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clearFromDegreeCourse();
                if (position == 0) {
                    return;
                }

                final DidacticPeriod didacticPeriod = (DidacticPeriod) parent.getItemAtPosition(position);
                Log.d("AddFragment", "Selected didactic period: " + didacticPeriod);

                selectedDidacticPeriod = didacticPeriod;
                showLoadingDialog();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        degreeCourseLiveData = viewModel.getDegreeCourses(didacticPeriod);
                        Handler mainHandler = new Handler(getContext().getMainLooper());
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                degreeCourseObserver = createDegreeCourseObserver();
                                degreeCourseLiveData.observe(fragment, degreeCourseObserver);
                                dismissLoadingDialog();
                                checkDidacticPeriodDataIsUpdated(viewModel, selectedDidacticPeriod);
                            }
                        });
                    }
                }).start();
                clearFromDegreeCourse();


                degreeSpinner.performClick();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }


    private AdapterView.OnItemSelectedListener createDegreeCourseClickListener(final SearchScheduleByCourseFragment fragment) {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                clearYear();
                if (position == 0) {
                    return;
                }

                DegreeCourse degreeCourse = (DegreeCourse) parent.getItemAtPosition(position);
                Log.d("AddFragment", "Selected degree course: " + degreeCourse);

                selectedDegreeCourse = degreeCourse;
                clearYear();
                yearLiveData = viewModel.getDegreeCourseYears(selectedDidacticPeriod, selectedDegreeCourse);
                yearObserver = createAcademicYearObserver();
                yearLiveData.observe(fragment, yearObserver);

                yearSpinner.performClick();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private AdapterView.OnItemSelectedListener createYearClickListener(final SearchScheduleByCourseFragment fragment) {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    clearYear();
                    return;
                }

                String year = (String) parent.getItemAtPosition(position);
                Log.d("AddFragment", "Selected year: " + year);

                selectedYear = year;
                courseLiveData = viewModel.getCoursesByDidacticPeriodAndYear(selectedDidacticPeriod, selectedDegreeCourse, selectedYear);
                courseObserver = createCourseObserver();
                courseLiveData.observe(fragment, courseObserver);
                setSearchButtonEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    public void touchView(View view) {
        MotionEvent m1 = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0);
        MotionEvent m2 = MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0);
        view.dispatchTouchEvent(m1);
        view.dispatchTouchEvent(m2);
        view.onTouchEvent(m1);
        view.onTouchEvent(m2);
    }

    private void clearFromFaculty() {
        Log.d("AddSchedule", "clearFromFaculty");
        selectedFaculty = null;
        facultySpinner.setSelection(0);
        clearFromDidacticPeriod();
    }

    private void clearFromDidacticPeriod() {
        Log.d("AddSchedule", "clearFromDidacticPeriod");
        selectedDidacticPeriod = null;
        didacticPeriodSpinner.setSelection(0);
        didactidPeriodAdapter.clear();
        didactidPeriodAdapter.add(didacticPeriodPlaceholder);
        didactidPeriodAdapter.notifyDataSetChanged();
        clearFromDegreeCourse();
    }

    private void clearFromDegreeCourse() {
        Log.d("AddSchedule", "clearFromDegreeCourse");
        selectedDegreeCourse = null;
        degreeSpinner.setSelection(0);
        degreeCourseAdapter.clear();
        degreeCourseAdapter.add(degreeCoursePlaceholder);
        didactidPeriodAdapter.notifyDataSetChanged();
        clearYear();
    }

    private void clearYear() {
        Log.d("AddSchedule", "clearFromYear");
        selectedYear = null;
        yearSpinner.setSelection(0);
        yearAdapter.clear();
        yearAdapter.add(yearPlaceholder);
        yearAdapter.notifyDataSetChanged();
        setSearchButtonEnabled(false);
    }

    private void setSearchButtonEnabled(boolean enabled) {
        searchButton.setEnabled(enabled);
        searchButton.setClickable(enabled);
        if (enabled) {
            searchButton.setAlpha(1);
        } else {
            searchButton.setAlpha(0.5f);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
//        removeAllObservers();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_schedule_by_course_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh_faculties_data:
                refreshUniversityData();
                clearFromFaculty();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void removeAllObservers() {
        facultyLiveData.removeObserver(facultyObserver);
        degreeCourseLiveData.removeObserver(degreeCourseObserver);
        yearLiveData.removeObserver(yearObserver);
    }

    private void refreshUniversityData() {
        showLoadingDialog();
        final Fragment fragment = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                facultyLiveData = viewModel.getFaculties(true);
                Handler mainHandler = new Handler(getContext().getMainLooper());
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        facultyLiveData.observe(fragment, facultyObserver); // si occupa dei DegreeCourse e AcademicYear
                        dismissLoadingDialog();
                        checkUniversityDataIsUpdated(viewModel);
                    }
                });
            }
        }).start();
    }

}
