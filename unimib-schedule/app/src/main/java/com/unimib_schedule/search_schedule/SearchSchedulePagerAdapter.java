package com.unimib_schedule.search_schedule;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class SearchSchedulePagerAdapter extends FragmentStatePagerAdapter {

    public SearchSchedulePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "CdL";
        } else if (position == 1) {
            return "Professore";
        }
        return "ORRORE";
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new SearchScheduleByCourseFragment();
        } else if (position == 1) {
            return new SearchScheduleByProfessorFragment();
        }

        return null;
    }

}