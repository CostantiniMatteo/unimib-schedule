package com.unimib_schedule.edit_schedule;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.unimib_schedule.R;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.edit_schedule.EditScheduleFragment.OnListFragmentInteractionListener;

import java.util.List;

public class EditScheduleRecyclerViewAdapter extends RecyclerView.Adapter<EditScheduleRecyclerViewAdapter.ViewHolder> {

    private final List<PersonalScheduleCourse> mValues;
    private final OnListFragmentInteractionListener mListener;

    public EditScheduleRecyclerViewAdapter(List<PersonalScheduleCourse> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_edit_schedule, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mCheckView.setChecked(holder.mItem.selected);
        holder.mNameView.setText(mValues.get(position).name);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }

                holder.mCheckView.setChecked(!holder.mItem.selected);
            }
        });

        holder.mCheckView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                holder.mItem.selected = isChecked;
            }
        });
    }

    public PersonalScheduleCourse getItem(int position) {
        return mValues.get(position);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final CheckBox mCheckView;
        public final TextView mNameView;
        public PersonalScheduleCourse mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCheckView = (CheckBox) view.findViewById(R.id.checkbox_course);
            mNameView = (TextView) view.findViewById(R.id.course_name);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
