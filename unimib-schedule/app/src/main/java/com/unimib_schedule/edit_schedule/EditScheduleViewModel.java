package com.unimib_schedule.edit_schedule;

import android.app.Application;

import androidx.annotation.NonNull;

import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.utils.BaseViewModel;

import java.util.List;

public class EditScheduleViewModel extends BaseViewModel {

    public EditScheduleViewModel(@NonNull Application application) {
        super(application);
    }

    public void updatePersonalSchedule(List<PersonalScheduleCourse> personalSchedule) {
        repository.updatePersonalSchedule(personalSchedule);
    }
}
