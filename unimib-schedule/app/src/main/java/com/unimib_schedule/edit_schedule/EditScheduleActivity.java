package com.unimib_schedule.edit_schedule;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.FragmentTransaction;

import com.unimib_schedule.R;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.utils.BaseActivity;

import java.util.ArrayList;

/***
 * Display a list of {PersonalScheduleCourse} passed with {PERSONAL_SCHEDULE_KEY} and allows
 * to select which one to display.
 */
public class EditScheduleActivity extends BaseActivity implements EditScheduleFragment.OnListFragmentInteractionListener {
    public static final String PERSONAL_SCHEDULE_KEY = "com.unimib_schedule.display_lectures.personal_schedule";
    private ArrayList<PersonalScheduleCourse> personalScheduleCourses;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        personalScheduleCourses = getIntent().getParcelableArrayListExtra(PERSONAL_SCHEDULE_KEY);

        Log.d("EDIT_SCHEDULE", personalScheduleCourses.toString());

        launchFragment();
    }

    private void launchFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        EditScheduleFragment fragment = new EditScheduleFragment(personalScheduleCourses);

        ft.replace(R.id.fragment, fragment);
        ft.commit();
    }

    @Override
    public void onListFragmentInteraction(PersonalScheduleCourse item) {

    }
}
