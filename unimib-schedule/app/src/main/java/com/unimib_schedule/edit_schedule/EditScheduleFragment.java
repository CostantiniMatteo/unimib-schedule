package com.unimib_schedule.edit_schedule;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.unimib_schedule.R;
import com.unimib_schedule.UnimibScheduleApplication;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.home.HomeActivity;
import com.unimib_schedule.utils.FragmentWithProgressDialog;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


/***
 * Fragment used to show the list of {PersonalScheduleCourse}
 */
public class EditScheduleFragment extends FragmentWithProgressDialog {
    private FloatingActionButton saveFab;
    private ArrayList<PersonalScheduleCourse> personalScheduleCourses;
    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private EditScheduleViewModel viewModel;

    public EditScheduleFragment() {
    }

    public EditScheduleFragment(ArrayList<PersonalScheduleCourse> personalScheduleCourses) {
        this.personalScheduleCourses = personalScheduleCourses;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_schedule, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @androidx.annotation.Nullable Bundle savedInstanceState) {
        viewModel = ViewModelProviders.of(this).get(EditScheduleViewModel.class);

        recyclerView = getActivity().findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        OnListFragmentInteractionListener listener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(PersonalScheduleCourse item) {

            }
        };

        // specify an adapter (see also next example)
        adapter = new EditScheduleRecyclerViewAdapter(personalScheduleCourses, listener);
        recyclerView.setAdapter(adapter);

        saveFab = getView().findViewById(R.id.saveFab);
        saveFab.setOnClickListener(createSaveFabClickListener());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        saveFab.setClickable(true);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(PersonalScheduleCourse item);
    }

    @NotNull
    private View.OnClickListener createSaveFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFab.setClickable(false);
                Log.d("EDIT SCHEDULE FRAGMENT", personalScheduleCourses.toString());
                UnimibScheduleApplication.triggerScheduleUpdaterReceiver(getContext(), false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        viewModel.updatePersonalSchedule(personalScheduleCourses);
                        Handler mainHandler = new Handler(getContext().getMainLooper());
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                startActivity(intent);
                            }
                        });
                    }
                }).start();
            }
        };
    }
}
