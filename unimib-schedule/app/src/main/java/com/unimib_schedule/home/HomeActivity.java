package com.unimib_schedule.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.unimib_schedule.R;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.display_lectures.DisplayLecturesFragment;
import com.unimib_schedule.search_schedule.SearchCourseActivity;
import com.unimib_schedule.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/***
 * Home activity that either show the saved schedule using {display_lectures.DisplayLecturesFragment}
 * or prompts to save a new schedule using {EmptyScheduleFragment}.
 */
public class HomeActivity extends BaseActivity implements  DisplayLecturesFragment.OnFragmentInteractionListener {
    public static final int ADD_FAB_ID = 667;
    private HomeActivityViewModel viewModel;
    private LiveData<List<PersonalScheduleCourse>> personalScheduleLiveData;
    private Observer<List<PersonalScheduleCourse>> personalScheduleCourseObserver;
    private LiveData<List<Course>> coursesLiveData;
    private Observer<List<Course>> courseObserver;
    private LiveData<List<Lecture>> scheduleLecturesLiveData;
    private Observer<List<Lecture>> lecturesObserver;

    private ArrayList<PersonalScheduleCourse> personalScheduleCourses;
    private ArrayList<Lecture> scheduleLectures;
    private ArrayList<Course> mCourses;

    private Fragment currentFragment;
    private ConstraintLayout constraintLayout;
    private TextView emptyScheduleTextView;
    private FloatingActionButton addFab;
    private LinearLayout linearLayout;

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        Log.d("HOME_ACTIVITY", "OnCreate");
        setTitle("Unimib Schedule");

        initializeTextViewAndAddFab();
    }

    @Override
    protected void onStart() {
        Log.d("HOME_ACTIVITY", "onStart");
        super.onStart();
    }

    private Observer<List<PersonalScheduleCourse>> createPersonalScheduleObserver() {
        return new Observer<List<PersonalScheduleCourse>>() {
            @Override
            public void onChanged(List<PersonalScheduleCourse> personalSchedule) {
                if (currentFragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(currentFragment).commit();
                    getSupportFragmentManager().executePendingTransactions();
                }
                if (personalSchedule.size() > 0) {
                    personalScheduleCourses = new ArrayList<>(personalSchedule);
                    observeCourses();
                } else {
                    showEmptySchedule();
                }
            }
        };
    }

    private void observeCourses() {
        List<String> courseIds = new ArrayList<>();
        for (PersonalScheduleCourse c : personalScheduleCourses) {
            if (c.selected) {
                courseIds.add(c.id);
            }
        }
        if (coursesLiveData != null && courseObserver != null) {
            coursesLiveData.removeObserver(courseObserver);
        }
        coursesLiveData = viewModel.getCoursesByIds(courseIds);

        courseObserver = createCoursesObserver();
        coursesLiveData.observe(this, courseObserver);
    }

    private Observer<List<Course>> createCoursesObserver() {
        return new Observer<List<Course>>() {
            @Override
            public void onChanged(List<Course> courses) {
                mCourses = new ArrayList<>(courses);
                observeLectures();
            }
        };
    }

    private void observeLectures() {
        if (scheduleLecturesLiveData != null) {
            scheduleLecturesLiveData.removeObservers(this);
        }

        scheduleLecturesLiveData = viewModel.getLecturesByCourses(mCourses);

        lecturesObserver = createScheduleLecturesObserver();
        scheduleLecturesLiveData.observe(this, lecturesObserver);
    }

    private Observer<List<Lecture>> createScheduleLecturesObserver() {
        return new Observer<List<Lecture>>() {
            @Override
            public void onChanged(List<Lecture> lectures) {
                scheduleLectures = new ArrayList<>(lectures);
                createDisplayLecturesFragment(scheduleLectures, mCourses);
            }
        };
    }

    private void showEmptySchedule() {
        linearLayout = findViewById(R.id.activity_base);
        FrameLayout frameLayout = findViewById(R.id.fragment);
        frameLayout.setVisibility(View.GONE);

        if (currentFragment != null) {
            getSupportFragmentManager().beginTransaction().remove(currentFragment).commitNow();
        }

        ViewParent view = constraintLayout.getParent();
        if (constraintLayout.getParent() == null) {
            linearLayout.addView(constraintLayout);
        }

        addFab.setClickable(true);
        addFab.setEnabled(true);
        Log.d("HOME_ACTIVITY", "showEmptySchedule tooltip.show()");

        constraintLayout.setVisibility(View.VISIBLE);
    }

    private void createDisplayLecturesFragment(ArrayList<Lecture> lectures, ArrayList<Course> courses) {
        Log.d("HOME_ACTIVITY", "createDisplayLectureFragment");
        if (constraintLayout.getParent() != null) {
            linearLayout = findViewById(R.id.activity_base);
            //linearLayout.removeView(constraintLayout);

            constraintLayout.setVisibility(View.GONE);

            FrameLayout frameLayout = findViewById(R.id.fragment);
            frameLayout.setVisibility(View.VISIBLE);
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        final DisplayLecturesFragment fragment = new DisplayLecturesFragment();

        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList(DisplayLecturesFragment.LECTURES_KEY, lectures);
        arguments.putParcelableArrayList(DisplayLecturesFragment.COURSES_KEY, courses);
        arguments.putBoolean(DisplayLecturesFragment.DELETE_ITEM_ENABLED, true);
        arguments.putBoolean(DisplayLecturesFragment.EDIT_FAB_ENABLED, true);
        fragment.setArguments(arguments);

        ft.replace(R.id.fragment, fragment);
        ft.commit();
        getSupportFragmentManager().executePendingTransactions();
        currentFragment = fragment;
    }

    @Override
    protected void onResume() {
        Log.d("HOME_ACTIVITY", "onResume");
        super.onResume();
        viewModel = ViewModelProviders.of(this).get(HomeActivityViewModel.class);
        if (personalScheduleLiveData == null) {
            personalScheduleLiveData = viewModel.getPersonalSchedule();
        }

        if (personalScheduleCourseObserver != null) {
            personalScheduleLiveData.removeObserver(personalScheduleCourseObserver);
        }

        personalScheduleCourseObserver = createPersonalScheduleObserver();
        personalScheduleLiveData.observe(this, personalScheduleCourseObserver);
    }

    @Override
    protected void onRestart() {
        Log.d("HOME_ACTIVITY", "onRestart");
        super.onRestart();
    }

    @SuppressLint("ResourceType")
    private void initializeTextViewAndAddFab() {
        Log.d("HOME_ACTIVITY", "initializeTextViewAndAddFab");
        LinearLayout.LayoutParams constraintParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );
        constraintLayout = new ConstraintLayout(this);
        constraintLayout.setLayoutParams(constraintParams);
        constraintLayout.setId(666);

        ConstraintLayout.LayoutParams textViewParams = new ConstraintLayout.LayoutParams(
                0,
                ConstraintLayout.LayoutParams.MATCH_PARENT
        );
        textViewParams.bottomToBottom = constraintLayout.getId();
        textViewParams.endToEnd = constraintLayout.getId();
        textViewParams.startToStart = constraintLayout.getId();
        textViewParams.topToTop = constraintLayout.getId();
        textViewParams.verticalBias = 0.21f;
        textViewParams.setMargins(100, 350, 100, 0);

        emptyScheduleTextView = new TextView(this);
        emptyScheduleTextView.setText(R.string.empty_schedule_message);
        emptyScheduleTextView.setLayoutParams(textViewParams);
        emptyScheduleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
        emptyScheduleTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        ConstraintLayout.LayoutParams fabParams = new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.WRAP_CONTENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
        );
        fabParams.setMargins(16, 16, 16, 16);
        fabParams.bottomToBottom = constraintLayout.getId();
        fabParams.endToEnd = constraintLayout.getId();
        fabParams.startToStart = constraintLayout.getId();
        fabParams.topToTop = constraintLayout.getId();
        fabParams.horizontalBias = 0.977f;
        fabParams.verticalBias = 0.977f;

        addFab = new FloatingActionButton(this);
        addFab.setId(ADD_FAB_ID);
        addFab.setLayoutParams(fabParams);
        addFab.setImageResource(R.drawable.ic_add_white_192dp);
        addFab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.fabColor)));

        final Activity activity = this;
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFab.setClickable(false);
                Intent intent = new Intent(activity, SearchCourseActivity.class);
                startActivity(intent);
            }
        });

        constraintLayout.addView(emptyScheduleTextView);
        constraintLayout.addView(addFab);


    }

    @Override
    protected void onStop() {
        Log.d("HOME_ACTIVITY", "onStop");
        super.onStop();
    }
}
