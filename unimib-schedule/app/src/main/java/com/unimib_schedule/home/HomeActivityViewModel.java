package com.unimib_schedule.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.utils.BaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class HomeActivityViewModel extends BaseViewModel {

    public HomeActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<PersonalScheduleCourse>> getPersonalSchedule() {
        return repository.getPersonalSchedule();
    }

    public LiveData<List<Lecture>> getLecturesByCourses(List<Course> courses) {
        List<String> courseIds = new ArrayList<>(courses.size());
        for (Course c : courses) {
            courseIds.add(c.id);
        }

        return repository.getLecturesByCourseIds(courseIds);
    }

    public LiveData<List<Course>> getCoursesByIds(List<String> courseIds) {
        return repository.getCoursesByCourseIds(courseIds);
    }
}
