package com.unimib_schedule.utils;

import android.app.ProgressDialog;

import com.unimib_schedule.R;

/***
 * Fragment that handles a ProgressDialog during remote calls.
 */
public class FragmentWithProgressDialog extends BaseFragment {
    protected ProgressDialog progressDialog;

    public void showLoadingDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle(getString(R.string.progressDialogTitle));
            progressDialog.setMessage(getString(R.string.progressDialogMessage));
        }
        progressDialog.show();
    }

    public void dismissLoadingDialog() {

        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onResume() {
        dismissLoadingDialog();
        super.onResume();
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }
}
