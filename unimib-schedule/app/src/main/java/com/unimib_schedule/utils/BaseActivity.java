package com.unimib_schedule.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;
import com.unimib_schedule.R;
import com.unimib_schedule.home.HomeActivity;
import com.unimib_schedule.search_schedule.SearchCourseActivity;

/***
 * Base Activity which should extend all other Activities.
 * The Drawer Menu logic is handled here.
 */
public abstract class BaseActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView drawerNavigationList;

    private boolean searchItemDisabled;
    private boolean homeItemDisabled;

    @Override
    protected void onStart() {
        Log.d("BASE_ACTIVITY", "onStart");
        super.onStart();
    }

    @Override
    public void setContentView(int layoutResID) {
        Log.d("BASE_ACTIVITY", "setContentView");
        super.setContentView(layoutResID);
        onCreateDrawer();
    }

    protected void onCreateDrawer() {
        drawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = drawerLayout.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
        setSupportActionBar(toolbar);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(drawerToggle);

        drawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        drawerNavigationList = findViewById(R.id.left_drawer);

        final Activity activity = this;
        drawerNavigationList.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.home:
                        if (!homeItemDisabled) {
                            homeItemDisabled = true;
                            Intent intent = new Intent(activity, HomeActivity.class);
                            startActivity(intent);
                        }
                        break;
                    case R.id.search:
                        if (!searchItemDisabled) {
                            searchItemDisabled = true;
                            launchSearchActivity();
                        }
                        break;
                    case R.id.language:
                        Toast.makeText(BaseActivity.this, "Language", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.notifications:
                        Toast.makeText(BaseActivity.this, "Notification", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        return true;
                }

                return true;
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    private void launchSearchActivity() {
        Intent intent = new Intent(this, SearchCourseActivity.class);
        startActivity(intent);
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        Log.d("BASE_ACTIVITY", "onPostCreate");
        super.onPostCreate(savedInstanceState, persistentState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        Log.d("BASE_ACTIVITY", "onResume");
        super.onResume();
        searchItemDisabled = false;
        homeItemDisabled = false;

        if (drawerToggle != null) {
            drawerToggle.syncState();
        }

        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onRestart() {
        Log.d("BASE_ACTIVITY", "onRestart");
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }
}
