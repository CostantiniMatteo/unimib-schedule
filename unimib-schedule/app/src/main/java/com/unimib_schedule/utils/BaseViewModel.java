package com.unimib_schedule.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.unimib_schedule.UnimibScheduleApplication;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Stat;
import com.unimib_schedule.data.source.Repository;

public class BaseViewModel extends AndroidViewModel {
    protected Repository repository;

    public BaseViewModel(@NonNull Application application) {
        super(application);
        repository = ((UnimibScheduleApplication) application).getRepository();
    }

    public LiveData<Stat> getLastUniversityRefreshStat() {
        return repository.getLastUniversityRefreshStat();
    }

    public LiveData<Stat> getLastDidacticPeriodRefreshStatSync(DidacticPeriod didacticPeriod) {
        return repository.getLastDidacticPeriodRefreshStat(didacticPeriod);
    }
}
