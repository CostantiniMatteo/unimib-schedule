package com.unimib_schedule.utils;

import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Stat;

/***
 * Base Fragment that handles warning for missing connections during data refresh.
 */
public class BaseFragment extends Fragment {
    private Observer<Stat> statObserver;
    private LiveData<Stat> statLiveData;

    protected void checkUniversityDataIsUpdated(BaseViewModel viewModel) {
        Log.d("BASE_FRAGMENT", "checkUniversityDataIsUpdated()");
        if (statLiveData != null) {
            statLiveData.removeObservers(this);
        }
        statLiveData = viewModel.getLastUniversityRefreshStat();
        statLiveData.observe(this, statObserver == null ? createObserver() : statObserver);
    }

    protected void checkDidacticPeriodDataIsUpdated(BaseViewModel viewModel, DidacticPeriod didacticPeriod) {
        Log.d("BASE_FRAGMENT", "checkDidacticPeriodDataIsUpdated(" + didacticPeriod.id + ")");
        if (statLiveData != null) {
            statLiveData.removeObservers(this);
        }
        statLiveData = viewModel.getLastDidacticPeriodRefreshStatSync(didacticPeriod);
        statLiveData.observe(this, statObserver == null ? createObserver() : statObserver);
    }

    private Observer<Stat> createObserver() {
        return new Observer<Stat>() {
            @Override
            public void onChanged(Stat stat) {
                Log.d("BASE_FRAGMENT", "Observer: " + this + "; Id: " + stat.id + "; Timestamp: " + stat.date + " (" + System.currentTimeMillis() + "); Success: " + stat.success);
                if (!stat.success) {
                    Log.d("BASE_FRAGMENT", "Un tooltip porca troia");
                    Toast.makeText(getContext(), "Connessione assente. Sto usando i vecchi dati.", Toast.LENGTH_LONG).show();
                }
            }
        };
    }

}
