package com.unimib_schedule.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

/***
 * Extension of the AutoCompleteTextView that shows all matching items even if there is no text
 * in the TextView
 */
public class InstantAutoCompleteTextView extends AppCompatAutoCompleteTextView {

    public InstantAutoCompleteTextView(Context context) {
        super(context);
    }

    public InstantAutoCompleteTextView(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
    }

    public InstantAutoCompleteTextView(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent arg0) {
        boolean result = super.onTouchEvent(arg0);

        showHints();

        return result;
    }

    public void showHints() {
        if (isFocused() && getFilter() != null) {
            performFiltering(getText(), 0);
            super.showDropDown();
        }
    }
}
