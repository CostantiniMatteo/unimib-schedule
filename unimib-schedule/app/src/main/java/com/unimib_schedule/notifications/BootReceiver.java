package com.unimib_schedule.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.unimib_schedule.UnimibScheduleApplication;

/***
 * Triggers {ScheduleUpdaterReceiver} on device reboot
 */
public class BootReceiver extends BroadcastReceiver {
    private static final int interval = 21600000;// 6 hours = 21 600 000 millis

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Log.d("BOOT_RECEIVER", "onReceive");
            UnimibScheduleApplication.triggerScheduleUpdaterReceiver(context, true);
        }
    }
}
