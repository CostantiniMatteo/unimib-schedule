package com.unimib_schedule.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.unimib_schedule.R;
import com.unimib_schedule.UnimibScheduleApplication;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.source.DataSource;
import com.unimib_schedule.data.source.Repository;
import com.unimib_schedule.data.source.local.Database;
import com.unimib_schedule.home.HomeActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/***
 * Receiver used to refresh the local data using the remote APIs and to manage notifications
 * both for upcoming lectures and changes of schedule.
 */
public class ScheduleUpdaterReceiver extends BroadcastReceiver {

    private static Comparator<Lecture> sortByDate = new Comparator<Lecture>() {
        @Override
        public int compare(Lecture o1, Lecture o2) {
            return o1.beginDate.before(o2.beginDate) ? 0 : 1;
        }
    };

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("RECEIVER", "onReceive");

        Database db = Database.getInstance(context);
        final DataSource repository = Repository.getInstance(
                db.didacticPeriodDao(),
                db.academicYearDao(),
                db.facultyDao(),
                db.degreeCourseDao(),
                db.professorDao(),
                db.courseDao(),
                db.professorTeachingCourseDao(),
                db.lectureDao(),
                db.personalScheduleCourseDao(),
                db.statDao()
        );

        new Thread(new Runnable() {
            @Override
            public void run() {

                // Recupera i dati correnti
                List<Course> courses = repository.getPersonalScheduleCoursesSync();

                List<String> courseIds = new ArrayList<>(courses.size());
                for (Course c : courses) {
                    courseIds.add(c.id);
                }

                List<Lecture> lectures = repository.getLecturesByCourseIdsSync(courseIds);
                List<DidacticPeriod> didacticPeriods = repository.getDidacticPeriodsByCourseIds(courseIds);

                // Aggiornare i dati
                repository.refreshUniversityData();
                for (DidacticPeriod d : didacticPeriods) {
                    repository.refreshDidacticPeriodData(d);
                }

                List<Lecture> updatedLectures = repository.getLecturesByCourseIdsSync(courseIds);

                // Confrontare con quelli vecchi
                Collections.sort(lectures, sortByDate);
                Collections.sort(updatedLectures, sortByDate);

                if (lectures.size() != updatedLectures.size()) {
                    sendScheduleChangedNotification(context);
                } else {
                    for (int i = 0; i < lectures.size(); i++) {
                        if (!lectures.get(i).equals(updatedLectures.get(i))) {
                            sendScheduleChangedNotification(context);
                            break;
                        }
                    }
                }

                // Gestire la coda di notifiche delle lezioni
                for (Lecture l : updatedLectures) {
                    setLectureNotification(context, l);
                }

            }
        }).start();
    }

    private void sendScheduleChangedNotification(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder b = new NotificationCompat.Builder(context, UnimibScheduleApplication.SCHEDULE_UPDATE_NOTIFICATION_CHANNEL);
        Notification notification = b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Cambiamento orario")
                .setContentText("Ci sono delle modifiche al tuo orario delle lezioni")
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis())
                .build();
        notificationManager.notify(1, notification);
    }

    private void setLectureNotification(Context context, Lecture lecture) {
        if (isAlarmSet(context, lecture.hashCode())) {
            Log.d("RECEIVER", lecture.name + " - " + lecture.building + " Already set");
            return;
        }

        if (Calendar.getInstance().getTime().after(lecture.beginDate)) {
            Log.d("RECEIVER", lecture.name + " - " + lecture.building + " Is in the past");
            return;
        }
        Log.d("RECEIVER", lecture.name + " - " + lecture.building + " Not set");

        Intent intent = new Intent(context, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder b = new NotificationCompat.Builder(context, UnimibScheduleApplication.LECTURES_NOTIFICATION_CHANNEL);
        Notification notification = b.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("C'hai una lezione tra poco")
                .setContentText(lecture.name + " - " + lecture.room)
                .setContentIntent(pendingIntent)
                .setWhen(lecture.beginDate.getTime() - 30 * 60 * 1000)
                .build();
        notificationManager.notify(lecture.hashCode(), notification);
    }

    private boolean isAlarmSet(Context context, int hashCode) {
        return PendingIntent.getBroadcast(context,
                hashCode,
                new Intent(context, ScheduleUpdaterReceiver.class),
                PendingIntent.FLAG_NO_CREATE
        ) != null;
    }
}
