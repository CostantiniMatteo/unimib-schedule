package com.unimib_schedule;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import com.unimib_schedule.data.source.Repository;
import com.unimib_schedule.data.source.local.Database;
import com.unimib_schedule.notifications.ScheduleUpdaterReceiver;

import java.util.Calendar;

public class UnimibScheduleApplication extends Application {
    public static final String SCHEDULE_UPDATE_NOTIFICATION_CHANNEL = "schedule_update_notification_channell";
    public static final String LECTURES_NOTIFICATION_CHANNEL = "lectures_notification_channel";

    @Override
    public void onCreate() {
        super.onCreate();
        createScheduleUpdateNotificationChannel();
        createLecturesNotificationChannel();
        setupFirstStart();
    }

    private void setupFirstStart() {
        SharedPreferences prefs = getSharedPreferences("com.unimib_schedule", MODE_PRIVATE);
        if (prefs.getBoolean("first_run", true)) {
            Log.d("APPLICATION", "First run");
            triggerScheduleUpdaterReceiver(this, true);
            prefs.edit().putBoolean("first_run", false).apply();
        }
    }

    public static void triggerScheduleUpdaterReceiver(Context context, boolean repeating) {
        Intent receiverIntent = new Intent(context, ScheduleUpdaterReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, receiverIntent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        if (repeating) {
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.add(Calendar.HOUR, 6); // first time
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 21000000, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    private void createScheduleUpdateNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Schedule Notification";
            String description = "Schedule Notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(SCHEDULE_UPDATE_NOTIFICATION_CHANNEL, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void createLecturesNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Lectures Notification";
            String description = "Lectures Notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(LECTURES_NOTIFICATION_CHANNEL, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public Repository getRepository() {
        Database db = Database.getInstance(getApplicationContext());
        return Repository.getInstance(
                db.didacticPeriodDao(),
                db.academicYearDao(),
                db.facultyDao(),
                db.degreeCourseDao(),
                db.professorDao(),
                db.courseDao(),
                db.professorTeachingCourseDao(),
                db.lectureDao(),
                db.personalScheduleCourseDao(),
                db.statDao()
        );
    }
}
