package com.unimib_schedule.data.source.local;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.unimib_schedule.data.model.AcademicYear;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.data.model.Professor;
import com.unimib_schedule.data.model.ProfessorTeachingCourse;
import com.unimib_schedule.data.model.Stat;

@androidx.room.Database(
        entities = {
                AcademicYear.class,
                Course.class,
                DegreeCourse.class,
                DidacticPeriod.class,
                Faculty.class,
                Lecture.class,
                PersonalScheduleCourse.class,
                Professor.class,
                ProfessorTeachingCourse.class,
                Stat.class,
        },
        version = 1,
        exportSchema = false)
public abstract class Database extends RoomDatabase {
    private static Database INSTANCE = null;

    public abstract FacultyDao facultyDao();

    public abstract CourseDao courseDao();

    public abstract AcademicYearDao academicYearDao();

    public abstract DegreeCourseDao degreeCourseDao();

    public abstract DidacticPeriodDao didacticPeriodDao();

    public abstract LectureDao lectureDao();

    public abstract ProfessorDao professorDao();

    public abstract ProfessorTeachingCourseDao professorTeachingCourseDao();

    public abstract PersonalScheduleCourseDao personalScheduleCourseDao();

    public abstract StatDao statDao();

    private static final Object sLock = new Object();

    public static Database getInstance(Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        Database.class, "unimib_schedule.db")
                        .build();
            }
            return INSTANCE;
        }
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
