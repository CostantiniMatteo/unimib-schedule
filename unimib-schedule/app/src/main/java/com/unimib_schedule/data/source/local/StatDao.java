package com.unimib_schedule.data.source.local;


import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Stat;


@Dao
public abstract class StatDao extends BaseDao<Stat> {
    private static final String LAST_UNIVERSITY_REFRESH = "LAST_UNIVERSITY_REFRESH";
    private static final String LAST_DIDACTIC_PERIOD_REFRESH = "LAST_DIDACTIC_PERIOD_REFRESH";

    @Query("SELECT * FROM Stat WHERE id = :id")
    public abstract LiveData<Stat> findById(String id);

    @Query("SELECT * FROM Stat WHERE id = :id")
    public abstract Stat findByIdSync(String id);


    public LiveData<Stat> getLastUniversityRefreshStat() {
        return findById(LAST_UNIVERSITY_REFRESH);
    }

    public Stat getLastUniversityRefreshStatSync() {
        return findByIdSync(LAST_UNIVERSITY_REFRESH);
    }

    public LiveData<Stat> getLastDidacticPeriodRefreshStat(DidacticPeriod didacticPeriod) {
        String id = getStatId(didacticPeriod);
        Log.d("STAT_DAO", "Fetching DidacticPeriod " + didacticPeriod.id + "; Id: " + id);
        return findById(id);
    }

    public Stat getLastDidacticPeriodRefreshStatSync(DidacticPeriod didacticPeriod) {
        String id = getStatId(didacticPeriod);
        return findByIdSync(id);
    }

    public void updateLastUniversityRefreshStat(long lastUpdate, boolean success) {
        upsert(new Stat(LAST_UNIVERSITY_REFRESH, lastUpdate, success));
    }

    public void updateLastDidacticPeriodRefreshStat(long lastUpdate, boolean success, DidacticPeriod didacticPeriod) {
        upsert(new Stat(
                getStatId(didacticPeriod),
                lastUpdate,
                success)
        );
    }

    private String getStatId(DidacticPeriod didacticPeriod) {
        return LAST_DIDACTIC_PERIOD_REFRESH + "|" + didacticPeriod.id;
    }
}
