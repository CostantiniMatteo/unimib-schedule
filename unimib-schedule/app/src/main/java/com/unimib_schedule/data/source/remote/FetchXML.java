package com.unimib_schedule.data.source.remote;

import org.w3c.dom.Document;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

class FetchXML {

    static Document fetch(String urlString) {
        urlString = urlString.replaceFirst("^http:", "https:");
        try {
            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(3000); // milliseconds
            conn.setReadTimeout(15000); // milliseconds
            InputStream is = conn.getInputStream();
            DocumentBuilderFactory factory =
                    DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(is);

            return doc;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
