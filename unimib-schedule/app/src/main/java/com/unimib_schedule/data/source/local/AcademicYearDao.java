package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.unimib_schedule.data.model.AcademicYear;

import java.util.List;

@Dao
public abstract class AcademicYearDao extends BaseDao<AcademicYear> {

    @Query("SELECT * FROM AcademicYear")
    public abstract LiveData<List<AcademicYear>> findAll();

    @Query("SELECT * FROM AcademicYear WHERE id = :academicYearId")
    public abstract LiveData<AcademicYear> findById(String academicYearId);

    @Query("SELECT * FROM AcademicYear WHERE id = :academicYearId")
    public abstract AcademicYear findByIdSync(String academicYearId);

    @Query("SELECT * FROM AcademicYear WHERE faculty = :facultyId")
    public abstract LiveData<List<AcademicYear>> findByFaculty(String facultyId);

    @Query("DELETE FROM AcademicYear")
    public abstract void deleteAll();

}
