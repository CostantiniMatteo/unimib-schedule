package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.unimib_schedule.data.model.Course;

import java.util.List;

@Dao
public abstract class CourseDao extends BaseDao<Course> {
    @Query("SELECT * FROM Course")
    public abstract LiveData<List<Course>> findAll();

    @Query("SELECT * FROM Course WHERE id = :courseId")
    public abstract LiveData<Course> findById(String courseId);

    @Query("SELECT * FROM Course WHERE id IN (:courseIds)")
    public abstract LiveData<List<Course>> findByIds(List<String> courseIds);

    @Query("SELECT * FROM Course WHERE didacticPeriod = :didacticPeriodId AND degreeCourse = :degreeCourseId AND year = :year")
    public abstract LiveData<List<Course>> findByDidacticPeriodDegreeCourseAndYear(String didacticPeriodId, String degreeCourseId, String year);

    @Query("SELECT * FROM Course WHERE Course.id IN " +
            "(SELECT ProfessorTeachingCourse.courseId FROM ProfessorTeachingCourse " +
            "WHERE ProfessorTeachingCourse.professorEmail = :professorEmail)")
    public abstract LiveData<List<Course>> findByProfessor(String professorEmail);

    @Query("SELECT * FROM course WHERE course.id IN " +
            "(SELECT PersonalScheduleCourse.id FROM PersonalScheduleCourse)")
    public abstract List<Course> getPersonalScheduleCourses();

    @Query("DELETE FROM Course")
    public abstract void deleteAll();
}
