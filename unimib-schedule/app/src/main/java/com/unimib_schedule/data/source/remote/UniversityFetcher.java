package com.unimib_schedule.data.source.remote;

import android.util.Log;

import com.unimib_schedule.data.model.AcademicYear;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;


/***
 * This class is used to fetch the list of faculties and their didactic periods from the remote APIs.
 */
public class UniversityFetcher implements Callable<UniversityResult> {
    private static final String FACULTIES_ENDPOINT = "https://gestioneorari.didattica.unimib.it/easy_app.php";
    private static final String UNIVERSITY_TAG = "university";
    private static final String FACULTY_TAG = "faculty";
    private static final String ACADEMIC_YEAR_TAG = "academicYear";
    private static final String DIDACTIC_PERIOD_TAG = "didacticPeriod";
    private static final String ID_ATTRIBUTE = "id";
    private static final String CODE_ATTRIBUTE = "code";
    private static final String NAME_ATTRIBUTE = "name";
    private static final String START_DATE_ATTRIBUTE = "start_date";
    private static final String END_DATE_ATTRIBUTE = "end_date";
    private static final String URL_ATTRIBUTE = "url";


    /***
     *
     * Fetches data from the remote API and returns the parsed data.
     *
     * @return
     * @throws Exception
     */
    public UniversityResult call() throws Exception {
        try {
            Document doc = FetchXML.fetch(FACULTIES_ENDPOINT);

            Element root = doc.getDocumentElement();
            Element university = (Element) root.getElementsByTagName(UNIVERSITY_TAG).item(0);

            List<Faculty> faculties = parseFaculties(university);
            List<AcademicYear> academicYears = parseAcademicYears(university);
            List<DidacticPeriod> didacticPeriods = parseDidacticPeriods(university);

            return new UniversityResult(faculties, academicYears, didacticPeriods);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private List<Faculty> parseFaculties(Element universityElement) {
        List<Faculty> faculties = new LinkedList<>();

        NodeList facultyNodes = universityElement.getElementsByTagName(FACULTY_TAG);
        for (int i = 0; i < facultyNodes.getLength(); i++) {
            Element facultyElement = (Element) facultyNodes.item(i);

            Faculty faculty = new Faculty();
            faculty.id = facultyElement.getAttribute(ID_ATTRIBUTE);
            faculty.code = facultyElement.getAttribute(CODE_ATTRIBUTE);
            faculty.name = facultyElement.getAttribute(NAME_ATTRIBUTE);

            Log.println(Log.DEBUG, "Faculty",
                    String.format("Id: %s; Name: %s; Code: %s",
                            faculty.id, faculty.name, faculty.code
                    )
            );

            faculties.add(faculty);
        }

        return faculties;
    }

    private List<AcademicYear> parseAcademicYears(Element universityElement) {
        List<AcademicYear> academicYears = new LinkedList<>();

        NodeList academicYearNodes = universityElement.getElementsByTagName(ACADEMIC_YEAR_TAG);
        for (int i = 0; i < academicYearNodes.getLength(); i++) {
            Element academicYearElement = (Element) academicYearNodes.item(i);

            if (academicYearElement.getChildNodes().getLength() == 0) {
                continue;
            }


            AcademicYear academicYear = new AcademicYear();
            academicYear.id = academicYearElement.getAttribute(ID_ATTRIBUTE);
            academicYear.name = academicYearElement.getAttribute(NAME_ATTRIBUTE);
            academicYear.faculty = ((Element) academicYearElement.getParentNode().getParentNode())
                    .getAttribute(ID_ATTRIBUTE);

            Log.println(Log.DEBUG, "AcademicYear",
                    String.format("Id: %s; Name: %s; Faculty: %s",
                            academicYear.id, academicYear.name, academicYear.faculty
                    )
            );

            academicYears.add(academicYear);
        }

        return academicYears;
    }

    private List<DidacticPeriod> parseDidacticPeriods(Element universityElement) {
        List<DidacticPeriod> didacticPeriods = new LinkedList<>();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);

        NodeList didacticPeriodNodes = universityElement.getElementsByTagName(DIDACTIC_PERIOD_TAG);
        for (int i = 0; i < didacticPeriodNodes.getLength(); i++) {
            Element didacticPeriodElement = (Element) didacticPeriodNodes.item(i);

            DidacticPeriod didacticPeriod = new DidacticPeriod();
            didacticPeriod.id = didacticPeriodElement.getAttribute(ID_ATTRIBUTE);
            didacticPeriod.name = didacticPeriodElement.getAttribute(NAME_ATTRIBUTE);
            didacticPeriod.url = didacticPeriodElement.getAttribute(URL_ATTRIBUTE);
            didacticPeriod.academicYear = ((Element) didacticPeriodElement.getParentNode())
                    .getAttribute(ID_ATTRIBUTE);
            didacticPeriod.faculty = ((Element) didacticPeriodElement.getParentNode().getParentNode().getParentNode())
                    .getAttribute(ID_ATTRIBUTE);
            try {
                didacticPeriod.startDate = df.parse(
                        didacticPeriodElement.getAttribute(START_DATE_ATTRIBUTE)
                );
                didacticPeriod.endDate = df.parse(
                        didacticPeriodElement.getAttribute(END_DATE_ATTRIBUTE)
                );
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.println(Log.DEBUG, "DidacticPeriod",
                    String.format("Id: %s; Name: %s; Faculty: %s; AcademicYear: %s; Url: %s",
                            didacticPeriod.id, didacticPeriod.name, didacticPeriod.faculty,
                            didacticPeriod.academicYear, didacticPeriod.url
                    )
            );

            didacticPeriods.add(didacticPeriod);
        }

        return didacticPeriods;
    }
}
