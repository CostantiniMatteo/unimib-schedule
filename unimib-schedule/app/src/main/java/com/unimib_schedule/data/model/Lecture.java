package com.unimib_schedule.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.TypeConverters;

import com.unimib_schedule.data.source.DateConverter;

import java.util.Date;
import java.util.Objects;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"room", "beginDate", "endDate"},
        foreignKeys = {
                @ForeignKey(entity = Course.class,
                        parentColumns = "id",
                        childColumns = "course",
                        onDelete = CASCADE
                )
        },
        indices = {@Index(value = "course")}
)
@TypeConverters(DateConverter.class)
public class Lecture implements Parcelable {
    @NonNull
    public String room;
    public String building;
    @NonNull
    public Date beginDate;

    @NonNull
    public Date endDate;
    public String partitioning;
    public String course;
    public String name;

    public Lecture() {
    }

    @Ignore
    public Lecture(String room, String building, Date beginDate, Date endDate, String partitioning, String course, String name) {
        this.room = room;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.building = building;
        this.partitioning = partitioning;
        this.course = course;
        this.name = name;
    }

    @Ignore
    public Lecture(Parcel parcel) {
        this.room = parcel.readString();
        this.building = parcel.readString();
        this.beginDate = DateConverter.toDate(parcel.readLong());
        this.endDate = DateConverter.toDate(parcel.readLong());
        this.partitioning = parcel.readString();
        this.course = parcel.readString();
        this.name = parcel.readString();
    }


    @Override
    public String toString() {
        return "Lecture{" +
                "room='" + room + '\'' +
                ", building='" + building + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", partitioning='" + partitioning + '\'' +
                ", course='" + course + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lecture lecture = (Lecture) o;
        return room.equals(lecture.room) &&
                Objects.equals(building, lecture.building) &&
                beginDate.equals(lecture.beginDate) &&
                endDate.equals(lecture.endDate) &&
                Objects.equals(partitioning, lecture.partitioning) &&
                Objects.equals(course, lecture.course) &&
                Objects.equals(name, lecture.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(room, building, beginDate, endDate, partitioning, course, name);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.room);
        dest.writeString(this.building);
        dest.writeLong(DateConverter.toTimestamp(this.beginDate));
        dest.writeLong(DateConverter.toTimestamp(this.endDate));
        dest.writeString(this.partitioning);
        dest.writeString(this.course);
        dest.writeString(this.name);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Lecture createFromParcel(Parcel source) {
            return new Lecture(source);
        }

        @Override
        public Lecture[] newArray(int size) {
            return new Lecture[size];
        }
    };
}
