package com.unimib_schedule.data.source;

import androidx.lifecycle.LiveData;

import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.data.model.Professor;
import com.unimib_schedule.data.model.Stat;

import java.util.List;

public interface DataSource {

    LiveData<List<Faculty>> getFaculty();

    LiveData<List<Faculty>> getFaculties(boolean forceRefresh);

    LiveData<Faculty> getFacultyByCode(String facultyCode);

    Faculty getFacultyByCodeSync(String facultyCode);

    LiveData<List<DegreeCourse>> getDegreeCourses(DidacticPeriod didacticPeriod);

    LiveData<List<DidacticPeriod>> getDidacticPeriodsByFaculty(Faculty faculty);

    List<DidacticPeriod> getDidacticPeriodsByCourseIds(List<String> courseIds);

    LiveData<List<String>> getDegreeCourseYears(DidacticPeriod didacticPeriod, DegreeCourse degreeCourse);

    LiveData<List<Professor>> getProfessors();

    LiveData<List<Course>> findByProfessor(Professor professor);

    LiveData<List<Course>> findByDidacticPeriodDegreeCourseAndYear(DidacticPeriod didacticPeriod, DegreeCourse degreeCourse, String year);

    LiveData<List<PersonalScheduleCourse>> getPersonalSchedule();

    List<Course> getPersonalScheduleCoursesSync();

    void deletePersonalSchedule();

    LiveData<List<Course>> getCoursesByCourseIds(List<String> courseIds);

    void updatePersonalSchedule(List<PersonalScheduleCourse> personalSchedule);

    LiveData<List<Lecture>> getLecturesByCourseIds(List<String> courseIds);

    List<Lecture> getLecturesByCourseIdsSync(List<String> courseIds);

    LiveData<Stat> getLastUniversityRefreshStat();

    LiveData<Stat> getLastDidacticPeriodRefreshStat(DidacticPeriod didacticPeriod);

    void refreshUniversityData();

    void refreshDidacticPeriodData(DidacticPeriod didacticPeriod);
}