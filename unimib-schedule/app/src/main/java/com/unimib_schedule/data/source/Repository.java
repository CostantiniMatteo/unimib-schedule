package com.unimib_schedule.data.source;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.unimib_schedule.data.model.AcademicYear;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.data.model.Professor;
import com.unimib_schedule.data.model.Stat;
import com.unimib_schedule.data.source.local.AcademicYearDao;
import com.unimib_schedule.data.source.local.CourseDao;
import com.unimib_schedule.data.source.local.DegreeCourseDao;
import com.unimib_schedule.data.source.local.DidacticPeriodDao;
import com.unimib_schedule.data.source.local.FacultyDao;
import com.unimib_schedule.data.source.local.LectureDao;
import com.unimib_schedule.data.source.local.PersonalScheduleCourseDao;
import com.unimib_schedule.data.source.local.ProfessorDao;
import com.unimib_schedule.data.source.local.ProfessorTeachingCourseDao;
import com.unimib_schedule.data.source.local.StatDao;
import com.unimib_schedule.data.source.remote.DidacticPeriodFetcher;
import com.unimib_schedule.data.source.remote.DidacticPeriodResult;
import com.unimib_schedule.data.source.remote.UniversityFetcher;
import com.unimib_schedule.data.source.remote.UniversityResult;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Repository implements DataSource {
    private static Repository INSTANCE = null;

    private DidacticPeriodDao didacticPeriodDao;
    private AcademicYearDao academicYearDao;
    private FacultyDao facultyDao;
    private DegreeCourseDao degreeCourseDao;
    private ProfessorDao professorDao;
    private CourseDao courseDao;
    private ProfessorTeachingCourseDao professorTeachingCourseDao;
    private LectureDao lectureDao;
    private PersonalScheduleCourseDao personalScheduleCourseDao;
    private StatDao statDao;

    private HashMap<String, Long> mLastDidacticPeriodFetchMap;
    private long mLastUniversityFetch;


    private Repository(DidacticPeriodDao didacticPeriodDao,
                       AcademicYearDao academicYearDao, FacultyDao facultyDao,
                       DegreeCourseDao degreeCourseDao, ProfessorDao professorDao,
                       CourseDao courseDao, ProfessorTeachingCourseDao professorTeachingCourseDao,
                       LectureDao lectureDao, PersonalScheduleCourseDao personalScheduleCourseDao,
                       StatDao statDao) {
        this.didacticPeriodDao = didacticPeriodDao;
        this.academicYearDao = academicYearDao;
        this.facultyDao = facultyDao;
        this.degreeCourseDao = degreeCourseDao;
        this.professorDao = professorDao;
        this.courseDao = courseDao;
        this.professorTeachingCourseDao = professorTeachingCourseDao;
        this.lectureDao = lectureDao;
        this.personalScheduleCourseDao = personalScheduleCourseDao;
        this.statDao = statDao;

        this.mLastDidacticPeriodFetchMap = new HashMap<>();
        this.mLastUniversityFetch = 0;
    }

    public static Repository getInstance(DidacticPeriodDao didacticPeriodDao,
                                         AcademicYearDao academicYearDao, FacultyDao facultyDao,
                                         DegreeCourseDao degreeCourseDao, ProfessorDao professorDao,
                                         CourseDao courseDao, ProfessorTeachingCourseDao professorTeachingCourseDao,
                                         LectureDao lectureDao, PersonalScheduleCourseDao personalScheduleCourseDao,
                                         StatDao statDao) {
        if (INSTANCE == null) {
            synchronized (Repository.class) {
                INSTANCE = new Repository(didacticPeriodDao, academicYearDao,
                        facultyDao, degreeCourseDao, professorDao, courseDao,
                        professorTeachingCourseDao, lectureDao, personalScheduleCourseDao,
                        statDao);
            }
        }
        return INSTANCE;
    }


    public LiveData<List<Faculty>> getFaculties() {
        return getFaculties(false);
    }

    public LiveData<List<Faculty>> getFaculties(boolean forceRefresh) {
        if (forceRefresh || shouldRefreshUniversityData()) {
            refreshUniversityData();
        }

        return facultyDao.findAll();
    }

    @Override
    public LiveData<List<Faculty>> getFaculty() {
        return null;
    }

    @Override
    public LiveData<Faculty> getFacultyByCode(String facultyCode) {
        return null;
    }

    @Override
    public Faculty getFacultyByCodeSync(String facultyCode) {
        return facultyDao.findByCodeSync(facultyCode);
    }

    @Override
    public LiveData<List<DegreeCourse>> getDegreeCourses(DidacticPeriod didacticPeriod) {
        if (shouldRefreshDidacticPeriodData(didacticPeriod)) {
            refreshDidacticPeriodData(didacticPeriod);
        }
        return degreeCourseDao.findByFaculty(didacticPeriod.faculty);
    }

    @Override
    public LiveData<List<DidacticPeriod>> getDidacticPeriodsByFaculty(Faculty faculty) {
        if (shouldRefreshUniversityData()) {
            refreshUniversityData();
        }
        return didacticPeriodDao.findByFaculty(faculty.id);
    }

    @Override
    public List<DidacticPeriod> getDidacticPeriodsByCourseIds(List<String> courseIds) {
        return didacticPeriodDao.findByCourseIdsSync(courseIds);
    }

    @Override
    public LiveData<List<String>> getDegreeCourseYears(DidacticPeriod didacticPeriod, DegreeCourse degreeCourse) {
        if (shouldRefreshUniversityData()) {
            refreshUniversityData();
        }
        if (shouldRefreshDidacticPeriodData(didacticPeriod)) {
            refreshDidacticPeriodData(didacticPeriod);
        }
        return degreeCourseDao.findDegreeCourseYears(degreeCourse.id);
    }

    @Override
    public LiveData<List<Professor>> getProfessors() {
        // NOTA: Il metodo è utilizzato per implementare la ricerca per professore
        //    Per delle limitazioni delle API la ricerca è "abbozzata" e molto limitata:
        //    I professori che vengono recuperati sono quelli già presenti nei dati locali
        //    e la stessa cosa vale per i corsi e le lezioni associate.
        //    La limitazione è un vincolo delle API esterne e per implementare la funzionalità
        //    senza impattare sulle prestazioni (sarebbe necessario scaricare _molti_ MB di dati
        //    sul dispositivo) sarebbe necessario rivedere l'architettura del sistema e aggiungere
        //    un server che si occupi di richiamare le API terze.
        return professorDao.findAll();
    }

    @Override
    public LiveData<List<Course>> findByProfessor(Professor professor) {
        // NOTA: Valgono le stesse considerazioni di `getProfessors`
        return courseDao.findByProfessor(professor.email);
    }

    @Override
    public LiveData<List<Course>> findByDidacticPeriodDegreeCourseAndYear(DidacticPeriod didacticPeriod, DegreeCourse degreeCourse, String year) {
        return courseDao.findByDidacticPeriodDegreeCourseAndYear(didacticPeriod.id, degreeCourse.id, year);
    }

    @Override
    public LiveData<List<PersonalScheduleCourse>> getPersonalSchedule() {
        return personalScheduleCourseDao.findAll();
    }

    @Override
    public List<Course> getPersonalScheduleCoursesSync() {
        return courseDao.getPersonalScheduleCourses();
    }

    @Override
    public void deletePersonalSchedule() {
        personalScheduleCourseDao.deleteAll();
    }

    @Override
    public LiveData<List<Course>> getCoursesByCourseIds(List<String> courseIds) {
        return courseDao.findByIds(courseIds);
    }

    @Override
    public void updatePersonalSchedule(List<PersonalScheduleCourse> personalSchedule) {
        personalScheduleCourseDao.updatePersonalSchedule(personalSchedule);
    }

    @Override
    public LiveData<List<Lecture>> getLecturesByCourseIds(List<String> courseIds) {
        return lectureDao.findByCourses(courseIds);
    }

    @Override
    public List<Lecture> getLecturesByCourseIdsSync(List<String> courseIds) {
        return lectureDao.findByCoursesSync(courseIds);
    }

    @Override
    public LiveData<Stat> getLastUniversityRefreshStat() {
        return statDao.getLastUniversityRefreshStat();
    }

    @Override
    public LiveData<Stat> getLastDidacticPeriodRefreshStat(DidacticPeriod didacticPeriod) {
        return statDao.getLastDidacticPeriodRefreshStat(didacticPeriod);
    }

    private boolean shouldRefreshUniversityData() {
        long fifteenMinutes = 900000; // 15 minutes = 900000 milliseconds
        return System.currentTimeMillis() > mLastUniversityFetch + fifteenMinutes;
    }

    private boolean shouldRefreshDidacticPeriodData(DidacticPeriod didacticPeriod) {
        long fifteenMinutes = 900000; // 15 minutes = 900000 milliseconds
        long lastUpdated = mLastDidacticPeriodFetchMap.containsKey(didacticPeriod.id) ?
                mLastDidacticPeriodFetchMap.get(didacticPeriod.id) : 0;
        return System.currentTimeMillis() > lastUpdated + fifteenMinutes;
    }

    public void refreshUniversityData() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        UniversityFetcher fetchUniversityTask = new UniversityFetcher();

        Future<UniversityResult> future = executorService.submit(fetchUniversityTask);
        long lastUpdated = System.currentTimeMillis();
        try {
            UniversityResult result = future.get(); // bloccante, questo thread si ferma in attesa
            executorService.shutdown(); // lascia il parchetto più pulito di come l'hai trovato

            facultyDao.insert(result.faculties);
            academicYearDao.insert(result.academicYears);
            didacticPeriodDao.insert(result.didacticPeriods);

            Log.println(Log.DEBUG, "REPOSITORY", "FETCH_UNIVERSITY: Database updated");
            mLastUniversityFetch = lastUpdated;

            statDao.updateLastUniversityRefreshStat(lastUpdated, true);
        } catch (Exception e) {
            statDao.updateLastUniversityRefreshStat(lastUpdated, false);
            Log.println(Log.DEBUG, "REPOSITORY", "FETCH_UNIVERSITY: Data Not Available");
            e.printStackTrace();
        }
    }

    public void refreshDidacticPeriodData(final DidacticPeriod didacticPeriod) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        DidacticPeriodFetcher fetchFacultyTask = new DidacticPeriodFetcher(didacticPeriod, this);

        Future<DidacticPeriodResult> future = executorService.submit(fetchFacultyTask);
        long lastUpdated = System.currentTimeMillis();
        try {
            DidacticPeriodResult result = future.get();
            executorService.shutdown();

            degreeCourseDao.insert(result.degreeCourses);
            professorDao.insert(result.professors);
            courseDao.insert(result.courses);
            professorTeachingCourseDao.insert(result.ptcList);
            lectureDao.insert(result.lectures);

            Log.println(Log.DEBUG, "REPOSITORY", "FETCH_UNIVERSITY: Database updated");

            mLastDidacticPeriodFetchMap.put(didacticPeriod.id, lastUpdated);

            statDao.updateLastDidacticPeriodRefreshStat(lastUpdated, true, didacticPeriod);

        } catch (Exception e) {
            statDao.updateLastDidacticPeriodRefreshStat(lastUpdated, false, didacticPeriod);
            e.printStackTrace();
            Log.println(Log.DEBUG, "REPOSITORY", "FETCH_UNIVERSITY: Data Not Available");
        }
    }

    public List<Lecture> getLecturesByDateRange(Date startDate, Date endDate) {
        return lectureDao.findAllByDateRangeSync(startDate, endDate);
    }

    public LiveData<List<AcademicYear>> getAcademicYears() {
        return academicYearDao.findAll();
    }

    public LiveData<List<AcademicYear>> getAcademicYearsByFaculty(Faculty faculty) {
        return academicYearDao.findByFaculty(faculty.id);
    }

}
