package com.unimib_schedule.data.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;


@Entity
public class Professor {
    @PrimaryKey
    @NonNull
    public String email;

    public String name;
    public String phoneNumber;

    public Professor() {
    }

    @Ignore
    public Professor(@NonNull String email, String name, String phoneNumber) {
        this.email = email;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Professor professor = (Professor) o;
        return email.equals(professor.email) &&
                Objects.equals(name, professor.name) &&
                Objects.equals(phoneNumber, professor.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, name, phoneNumber);
    }
}
