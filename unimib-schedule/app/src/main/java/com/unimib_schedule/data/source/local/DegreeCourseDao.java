package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.unimib_schedule.data.model.DegreeCourse;

import java.util.List;

@Dao
public abstract class DegreeCourseDao extends BaseDao<DegreeCourse> {
    @Query("SELECT * FROM DegreeCourse")
    public abstract LiveData<List<DegreeCourse>> findAll();

    @Query("SELECT * FROM DegreeCourse WHERE id = :degreeCourseId")
    public abstract LiveData<DegreeCourse> findById(String degreeCourseId);

    @Query("SELECT * FROM DegreeCourse WHERE faculty = :facultyId")
    public abstract LiveData<List<DegreeCourse>> findByFaculty(String facultyId);

    @Query("SELECT DISTINCT year FROM Course WHERE degreeCourse = :degreeCourseId")
    public abstract LiveData<List<String>> findDegreeCourseYears(String degreeCourseId);

    @Query("DELETE FROM DegreeCourse")
    public abstract void deleteAll();
}
