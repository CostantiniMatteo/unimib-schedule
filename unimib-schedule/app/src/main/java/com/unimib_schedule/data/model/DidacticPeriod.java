package com.unimib_schedule.data.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.unimib_schedule.data.source.DateConverter;

import java.util.Date;
import java.util.Objects;

import static androidx.room.ForeignKey.CASCADE;


@Entity(foreignKeys = {
        @ForeignKey(entity = AcademicYear.class,
                parentColumns = "id",
                childColumns = "academicYear",
                onDelete = CASCADE),
        @ForeignKey(entity = Faculty.class,
                parentColumns = "id",
                childColumns = "faculty",
                onDelete = CASCADE)

},
        indices = {@Index(value = "academicYear"), @Index(value = "faculty")}
)
@TypeConverters(DateConverter.class)
public class DidacticPeriod {
    @PrimaryKey
    @NonNull
    public String id;
    public String name;
    public String faculty;
    public Date startDate;
    public Date endDate;
    public String academicYear;
    public String url;

    public DidacticPeriod() {
    }

    @Ignore
    public DidacticPeriod(@NonNull String id, String name, String faculty, Date startDate, Date endDate, String academicYear, String url) {
        this.id = id;
        this.name = name;
        this.faculty = faculty;
        this.startDate = startDate;
        this.endDate = endDate;
        this.academicYear = academicYear;
        this.url = url;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DidacticPeriod that = (DidacticPeriod) o;
        return id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(faculty, that.faculty) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(academicYear, that.academicYear) &&
                Objects.equals(url, that.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, faculty, startDate, endDate, academicYear, url);
    }
}
