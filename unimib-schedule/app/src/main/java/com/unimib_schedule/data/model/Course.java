package com.unimib_schedule.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys = {
        @ForeignKey(entity = DegreeCourse.class,
                parentColumns = "id",
                childColumns = "degreeCourse",
                onDelete = CASCADE
        ),
        @ForeignKey(entity = Faculty.class,
                parentColumns = "id",
                childColumns = "faculty",
                onDelete = CASCADE
        ),
        @ForeignKey(entity = DidacticPeriod.class,
                parentColumns = "id",
                childColumns = "didacticPeriod",
                onDelete = CASCADE),
},
        indices = {@Index(value = "degreeCourse"), @Index(value = "faculty"), @Index(value = "didacticPeriod")}
)
public class Course implements Parcelable {
    @NonNull
    @PrimaryKey
    public String id;
    public String code;
    public String name;
    public String faculty;
    public String year;
    public String semester;
    public String degreeCourse;
    public String didacticPeriod;

    @Ignore
    public Course(@NonNull String id, String code, String name, String faculty, String year,
                  String semester, String degreeCourse, String didacticPeriod) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.faculty = faculty;
        this.year = year;
        this.semester = semester;
        this.degreeCourse = degreeCourse;
        this.didacticPeriod = didacticPeriod;
    }

    @Ignore
    public Course(Parcel parcel) {
        this.id = parcel.readString();
        this.code = parcel.readString();
        this.name = parcel.readString();
        this.faculty = parcel.readString();
        this.year = parcel.readString();
        this.semester = parcel.readString();
        this.degreeCourse = parcel.readString();
        this.didacticPeriod = parcel.readString();
    }

    public Course() {
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return code.equals(course.code) &&
                id.equals(course.id) &&
                Objects.equals(name, course.name) &&
                Objects.equals(faculty, course.faculty) &&
                Objects.equals(year, course.year) &&
                Objects.equals(semester, course.semester) &&
                Objects.equals(degreeCourse, course.degreeCourse) &&
                Objects.equals(didacticPeriod, course.didacticPeriod);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, id, name, faculty, year, semester, degreeCourse, didacticPeriod);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.code);
        dest.writeString(this.name);
        dest.writeString(this.faculty);
        dest.writeString(this.year);
        dest.writeString(this.semester);
        dest.writeString(this.degreeCourse);
        dest.writeString(this.didacticPeriod);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Course createFromParcel(Parcel source) {
            return new Course(source);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}
