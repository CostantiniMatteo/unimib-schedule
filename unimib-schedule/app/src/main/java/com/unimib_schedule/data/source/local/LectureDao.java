package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.TypeConverters;

import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.source.DateConverter;

import java.util.Date;
import java.util.List;

@Dao
@TypeConverters(DateConverter.class)
public abstract class LectureDao extends BaseDao<Lecture> {
    @Query("SELECT * FROM Lecture")
    public abstract LiveData<List<Lecture>> findAll();

    @Query("SELECT * FROM Lecture where :fromDate < beginDate AND beginDate < :toDate")
    public abstract LiveData<List<Lecture>> findAllByDateRange(Date fromDate, Date toDate);

    @Query("SELECT * FROM Lecture where :fromDate < beginDate AND beginDate < :toDate")
    public abstract List<Lecture> findAllByDateRangeSync(Date fromDate, Date toDate);

    @Query("SELECT * FROM Lecture WHERE course = :courseId")
    public abstract LiveData<List<Lecture>> findByCourse(String courseId);

    @Query("SELECT * FROM Lecture WHERE course IN (:courseIds)")
    public abstract LiveData<List<Lecture>> findByCourses(List<String> courseIds);

    @Query("SELECT * FROM Lecture WHERE course IN (:courseIds)")
    public abstract List<Lecture> findByCoursesSync(List<String> courseIds);


    @Query("SELECT * FROM Lecture WHERE Lecture.course " +
            "IN (SELECT ProfessorTeachingCourse.courseId FROM ProfessorTeachingCourse " +
            "WHERE ProfessorTeachingCourse.professorEmail = :professorEmail)")
    public abstract LiveData<List<Lecture>> findByProfessor(String professorEmail);

    @Query("DELETE FROM Lecture")
    public abstract void deleteAll();

}
