package com.unimib_schedule.data.model;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Stat {
    @NonNull
    @PrimaryKey
    public String id;
    @NonNull
    public long date;
    public boolean success;

    public Stat() {
    }

    @Ignore
    public Stat(@NonNull String id, long date, boolean success) {
        this.id = id;
        this.date = date;
        this.success = success;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stat stat = (Stat) o;
        return date == stat.date &&
                success == stat.success &&
                id.equals(stat.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, success);
    }

    @Override
    public String toString() {
        return "Stat{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", success=" + success +
                '}';
    }
}
