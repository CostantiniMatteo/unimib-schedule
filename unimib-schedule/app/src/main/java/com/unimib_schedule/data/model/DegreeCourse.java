package com.unimib_schedule.data.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys =
@ForeignKey(entity = Faculty.class,
        parentColumns = "id",
        childColumns = "faculty",
        onDelete = CASCADE),
        indices = {@Index(value = "faculty")}
)
public class DegreeCourse {
    @PrimaryKey
    @NonNull
    public String id;
    public String name;
    public String faculty;

    public DegreeCourse() {
    }

    @Ignore
    public DegreeCourse(@NonNull String id, String name, String faculty) {
        this.id = id;
        this.name = name;
        this.faculty = faculty;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DegreeCourse that = (DegreeCourse) o;
        return id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(faculty, that.faculty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, faculty);
    }
}
