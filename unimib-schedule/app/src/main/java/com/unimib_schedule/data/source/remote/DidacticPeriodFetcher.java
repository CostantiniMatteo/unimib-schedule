package com.unimib_schedule.data.source.remote;

import android.util.Log;

import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.Professor;
import com.unimib_schedule.data.model.ProfessorTeachingCourse;
import com.unimib_schedule.data.source.DataSource;
import com.unimib_schedule.data.source.Repository;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;


/***
 * This class is used to fetch all the data of a DidacticPeriod from the external APIs.
 * The data parsed is listed in {DidacticPeriodResult}.
 */
public class DidacticPeriodFetcher implements Callable<DidacticPeriodResult> {
    private static final String SCHEDULE_TAG = "Orario";
    private static final String COURSE_TAG = "Insegnamento";
    private static final String MANIFESTO_TAG = "Manifesto";
    private static final String CURRICULUM_TAG = "Curriculum";
    private static final String PROFESSOR_TAG = "DocenteTitolare";
    private static final String LECTURES_CALENDAR_TAG = "CalendarioLezioni";
    private static final String LECTURE_TAG = "Giorno";
    private static final String GENERAL_CODE_ATTRIBUTE = "CodiceGenerale"; // F1801Q151
    private static final String NAME_ATTRIBUTE = "Nome";
    private static final String SURNAME_ATTRIBUTE = "Cognome";
    private static final String CODE_ATTRIBUTE = "Codice";
    private static final String SEMESTER_ATTRIBUTE = "PeriodoCodice";
    private static final String DEGREE_CODE_ATTRIBUTE = "LaureaCodice";
    private static final String DEGREE_NAME_ATTRIBUTE = "LaureaNome";
    private static final String DEGREE_TYPE_ATTRIBUTE = "LaureaTipo";
    private static final String FACULTY_CODE_ATTRIBUTE = "FacoltaCodice";
    private static final String PROFESSOR_MAIL_ATTRIBUTE = "Mail1";
    private static final String PROFESSOR_PHONE_ATTRIBUTE = "Fisso";
    private static final String ROOM_ATTRIBUTE = "Aula";
    private static final String BUILDING_ATTRIBUTE = "Sede";
    private static final String LECTURE_DAY_ATTRIBUTE = "Data";
    private static final String LECTURE_BEGIN_ATTRIBUTE = "OraInizio";
    private static final String LECTURE_END_ATTRIBUTE = "OraFine";
    private static final String PARTITIONING_ATTRIBUTE = "Nome";
    private static final String COURSE_YEAR_ATTRIBUTE = "AnnoCorso";


    private DidacticPeriod mDidacticPeriod;
    private DataSource mRepository;
    private DidacticPeriodResult mResult;

    public DidacticPeriodFetcher(DidacticPeriod didacticPeriod, Repository repository) {
        this.mDidacticPeriod = didacticPeriod;
        this.mRepository = repository;
        this.mResult = new DidacticPeriodResult();
    }


    /***
     *
     * Fetches data from the remote API and returns the parsed data.
     *
     * @return
     * @throws Exception
     */
    public DidacticPeriodResult call() throws Exception {
        try {
            Document doc = FetchXML.fetch(mDidacticPeriod.url);
            Element scheduleElement = (Element) doc.getElementsByTagName(SCHEDULE_TAG).item(0);

            parseCourses(scheduleElement);

            return mResult;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private List<Course> parseCourses(Element scheduleElement) {
        List<Course> courses = new LinkedList<>();
        List<ProfessorTeachingCourse> ptcList = new LinkedList<>();
        List<Lecture> lectures = new LinkedList<>();

        NodeList courseNodes = scheduleElement.getElementsByTagName(COURSE_TAG);
        for (int i = 0; i < courseNodes.getLength(); i++) {
            Element courseElement = (Element) courseNodes.item(i);

            String facultyId = mRepository.getFacultyByCodeSync(
                    ((Element) courseElement.getParentNode()).getAttribute(FACULTY_CODE_ATTRIBUTE)
            ).id;

            Element manifesto = (Element) courseElement.getElementsByTagName(MANIFESTO_TAG).item(0);
            Element curriculum = (Element) manifesto.getElementsByTagName(CURRICULUM_TAG).item(0);

            DegreeCourse degreeCourse = parseDegreeCourse(curriculum, facultyId);
            List<Professor> professors = parseProfessors(courseElement);

            Course course = new Course();
            course.code = courseElement.getAttribute(GENERAL_CODE_ATTRIBUTE);
            course.name = courseElement.getAttribute(NAME_ATTRIBUTE);
            course.degreeCourse = degreeCourse.id;
            course.faculty = facultyId;
            course.semester = ((Element) courseElement.getParentNode()).getAttribute(SEMESTER_ATTRIBUTE);
            course.year = curriculum.getAttribute(COURSE_YEAR_ATTRIBUTE);
            course.didacticPeriod = mDidacticPeriod.id;
            course.id = course.code + "|" + course.degreeCourse;

            Log.println(Log.DEBUG, "Fetcher",
                    String.format("Course (Id: %s; Name: %s; DegreeCourse: %s; Faculty: %s; Semester: %s; Year: %s; DidacticPeriod: %s)",
                            course.id, course.name, course.degreeCourse, course.faculty, course.semester, course.year, course.didacticPeriod
                    )
            );

            for (Professor professor : professors) {
                ProfessorTeachingCourse ptc = new ProfessorTeachingCourse();
                ptc.courseId = course.id;
                ptc.professorEmail = professor.email;
                ptcList.add(ptc);
            }

            Element lecturesCalendar = (Element) courseElement
                    .getElementsByTagName(LECTURES_CALENDAR_TAG).item(0);
            String partitioning = curriculum.getAttribute(PARTITIONING_ATTRIBUTE);

            List<Lecture> courseLectures = parseLectures(lecturesCalendar, course, partitioning);
            if (courseLectures.size() > 0) {
                lectures.addAll(courseLectures);
                courses.add(course);
            }
        }

        mResult.courses = courses;
        mResult.ptcList = ptcList;
        mResult.lectures = lectures;
        return courses;
    }

    private List<Professor> parseProfessors(Element courseElement) {
        List<Professor> professors = new LinkedList<>();

        Log.d("Fetcher", "Parsing professors for courseElement: " + courseElement.toString());
        NodeList professorNodes = courseElement.getElementsByTagName(PROFESSOR_TAG);

        for (int i = 0; i < professorNodes.getLength(); i++) {
            Element professorElement = (Element) professorNodes.item(i);

            Professor professor = new Professor();
            professor.email = professorElement.getAttribute(PROFESSOR_MAIL_ATTRIBUTE);
            professor.name = getCapitalizedFullName(professorElement.getAttribute(NAME_ATTRIBUTE),
                    professorElement.getAttribute(SURNAME_ATTRIBUTE));
            professor.phoneNumber = professorElement.getAttribute(PROFESSOR_PHONE_ATTRIBUTE);

            Log.println(Log.DEBUG, "Professor",
                    String.format("Name: %s; Email: %s; Phone: %s",
                            professor.name, professor.email, professor.phoneNumber
                    )
            );

            professors.add(professor);
        }

        mResult.professors.addAll(professors);
        return professors;
    }


    private DegreeCourse parseDegreeCourse(Element curriculum, String facultyId) {
        Log.d("Fetcher", "Parsing degree course for faculty " + facultyId);
        DegreeCourse degreeCourse = new DegreeCourse();
        degreeCourse.id = curriculum.getAttribute(DEGREE_CODE_ATTRIBUTE) + "|" + curriculum.getAttribute(CODE_ATTRIBUTE);
        degreeCourse.faculty = facultyId;

        String degreeName = curriculum.getAttribute(DEGREE_NAME_ATTRIBUTE);
        String partitioning = formatPartitioning(curriculum.getAttribute(NAME_ATTRIBUTE));
        if (partitioning.isEmpty()) {
            degreeCourse.name = degreeName + " (" + curriculum.getAttribute(DEGREE_TYPE_ATTRIBUTE) + ")";
        } else {
            degreeCourse.name = degreeName + " | " + partitioning + " ("
                    + curriculum.getAttribute(DEGREE_TYPE_ATTRIBUTE) + ")";
        }

        Log.println(Log.DEBUG, "Fetcher",
                String.format("DegreeCourse (Id: %s; Name: %s; Faculty: %s)",
                        degreeCourse.id, degreeCourse.name, degreeCourse.faculty
                )
        );

        mResult.degreeCourses.add(degreeCourse);
        return degreeCourse;
    }

    private String formatPartitioning(String partitioning) {
        if (partitioning == null) {
            return "";
        }
        return partitioning.replaceAll("\\s*[Pp]ercorso [Cc]omune\\s*", "");
    }

    private List<Lecture> parseLectures(Element lecturesCalendar, Course course, String partitioning) {
        Log.d("Fetcher", "Parsing lectures for course " + course.name + " (" + partitioning + ")");

        List<Lecture> lectures = new LinkedList<>();

        if (lecturesCalendar == null) {
            return lectures;
        }

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ITALIAN);

        NodeList lectureNodes = lecturesCalendar.getElementsByTagName(LECTURE_TAG);

        for (int i = 0; i < lectureNodes.getLength(); i++) {
            Element lectureElement = (Element) lectureNodes.item(i);

            String day = lectureElement.getAttribute(LECTURE_DAY_ATTRIBUTE);
            String beginHour = lectureElement.getAttribute(LECTURE_BEGIN_ATTRIBUTE);
            String endHour = lectureElement.getAttribute(LECTURE_END_ATTRIBUTE);

            Lecture lecture = new Lecture();
            lecture.room = lectureElement.getAttribute(ROOM_ATTRIBUTE);
            lecture.building = lectureElement.getAttribute(BUILDING_ATTRIBUTE);
            lecture.partitioning = partitioning;
            lecture.course = course.id;
            lecture.name = course.name;
            try {
                lecture.beginDate = df.parse(day + " " + beginHour);
                lecture.endDate = df.parse(day + " " + endHour);
            } catch (Exception e) {
                e.printStackTrace();
            }

            Log.println(Log.DEBUG, "Fetcher",
                    String.format("Lectures (Course: %s; Room: %s; Building: %s)",
                            lecture.course, lecture.room, lecture.building
                    )
            );

            lectures.add(lecture);
        }

        return lectures;
    }

    private String getCapitalizedFullName(String name, String surname) {
        String[] split = (name + " " + surname).toLowerCase().split(" ");
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < split.length; i++) {
            if (split[i].isEmpty()) {
                continue;
            }
            buffer.append(Character.toUpperCase(split[i].charAt(0)))
                    .append(split[i].substring(1));

            if (i != split.length - 1) {
                buffer.append(" ");
            }
        }
        return buffer.toString();
    }

}
