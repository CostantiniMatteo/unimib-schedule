package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import com.unimib_schedule.data.model.PersonalScheduleCourse;

import java.util.List;

@Dao
public abstract class PersonalScheduleCourseDao extends BaseDao<PersonalScheduleCourse> {
    @Transaction
    public void updatePersonalSchedule(List<PersonalScheduleCourse> personalSchedule) {
        deleteAll();
        insert(personalSchedule);
    }

    @Query("SELECT * FROM PersonalScheduleCourse")
    public abstract LiveData<List<PersonalScheduleCourse>> findAll();

    @Query("DELETE from PersonalScheduleCourse")
    public abstract void deleteAll();
}
