package com.unimib_schedule.data.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Objects;

import static androidx.room.ForeignKey.CASCADE;

@Entity(foreignKeys =
@ForeignKey(entity = Faculty.class,
        parentColumns = "id",
        childColumns = "faculty",
        onDelete = CASCADE),
        indices = {@Index(value = "faculty")}
)

public class AcademicYear {
    @PrimaryKey
    @NonNull
    public String id;
    public String name;
    public String faculty;

    @Ignore
    public AcademicYear(@NonNull String id, String name, String faculty) {
        this.id = id;
        this.name = name;
        this.faculty = faculty;
    }

    public AcademicYear() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AcademicYear that = (AcademicYear) o;
        return id.equals(that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(faculty, that.faculty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, faculty);
    }
}
