package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.unimib_schedule.data.model.Professor;

import java.util.List;

@Dao
public abstract class ProfessorDao extends BaseDao<Professor> {
    @Query("SELECT * FROM Professor")
    public abstract LiveData<List<Professor>> findAll();

    @Query("SELECT * FROM Professor WHERE email = :email")
    public abstract LiveData<Professor> findByEmail(String email);

    @Query("DELETE FROM Professor")
    public abstract void deleteAll();

}
