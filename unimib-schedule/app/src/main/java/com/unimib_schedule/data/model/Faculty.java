package com.unimib_schedule.data.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Faculty {

    @PrimaryKey
    @NonNull
    public String id;
    public String code;
    public String name;

    public Faculty() {
    }

    @Ignore
    public Faculty(@NonNull String id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Faculty faculty = (Faculty) o;
        return id.equals(faculty.id) &&
                Objects.equals(code, faculty.code) &&
                Objects.equals(name, faculty.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name);
    }
}
