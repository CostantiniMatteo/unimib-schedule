package com.unimib_schedule.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity(
        foreignKeys = {
                @ForeignKey(
                        entity = Course.class,
                        parentColumns = "id",
                        childColumns = "id",
                        onDelete = ForeignKey.CASCADE
                )
        }
)
public class PersonalScheduleCourse implements Parcelable {
    @PrimaryKey
    @NonNull
    public String id;
    public String name;
    public boolean selected;

    public PersonalScheduleCourse() {

    }

    @Ignore
    public PersonalScheduleCourse(String id, String name, boolean selected) {
        this.id = id;
        this.name = name;
        this.selected = selected;
    }

    @Ignore
    public PersonalScheduleCourse(Course course, boolean selected) {
        this(course.id, course.name, selected);
    }


    @Ignore
    public PersonalScheduleCourse(Parcel parcel) {
        this.id = parcel.readString();
        this.name = parcel.readString();
        this.selected = parcel.readInt() == 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonalScheduleCourse that = (PersonalScheduleCourse) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.selected ? 1 : 0);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public PersonalScheduleCourse createFromParcel(Parcel source) {
            return new PersonalScheduleCourse(source);
        }

        @Override
        public PersonalScheduleCourse[] newArray(int size) {
            return new PersonalScheduleCourse[size];
        }
    };

    @Override
    public String toString() {
        return "PersonalScheduleCourse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", selected=" + selected +
                '}';
    }
}
