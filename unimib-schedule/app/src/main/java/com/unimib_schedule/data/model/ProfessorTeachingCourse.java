package com.unimib_schedule.data.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;

import java.util.Objects;

@Entity(primaryKeys = {"courseId", "professorEmail"},
        foreignKeys = {
                @ForeignKey(entity = Course.class,
                        parentColumns = "id",
                        childColumns = "courseId"),
                @ForeignKey(entity = Professor.class,
                        parentColumns = "email",
                        childColumns = "professorEmail")
        },
        indices = {@Index(value = "professorEmail")})
public class ProfessorTeachingCourse {
    @NonNull
    public String courseId;
    @NonNull
    public String professorEmail;

    public ProfessorTeachingCourse() {
    }

    @Ignore
    public ProfessorTeachingCourse(String courseId, String professorEmail) {
        this.courseId = courseId;
        this.professorEmail = professorEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfessorTeachingCourse that = (ProfessorTeachingCourse) o;
        return courseId.equals(that.courseId) &&
                professorEmail.equals(that.professorEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseId, professorEmail);
    }
}
