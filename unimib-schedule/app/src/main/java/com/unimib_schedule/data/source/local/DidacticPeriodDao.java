package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.TypeConverters;

import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.source.DateConverter;

import java.util.List;

@Dao
@TypeConverters(DateConverter.class)
public abstract class DidacticPeriodDao extends BaseDao<DidacticPeriod> {
    @Query("SELECT * FROM DidacticPeriod")
    public abstract LiveData<List<DidacticPeriod>> findAll();

    @Query("SELECT * FROM DidacticPeriod WHERE id = :didacticPeriodId")
    public abstract LiveData<DidacticPeriod> findById(String didacticPeriodId);

    @Query("SELECT * FROM DidacticPeriod WHERE faculty = :facultyId")
    public abstract LiveData<List<DidacticPeriod>> findByFaculty(String facultyId);

    @Query("SELECT * FROM DidacticPeriod WHERE id IN " +
            "(SELECT Course.didacticPeriod FROM Course WHERE Course.id in (:courseIds))")
    public abstract List<DidacticPeriod> findByCourseIdsSync(List<String> courseIds);

    @Query("DELETE FROM DidacticPeriod")
    public abstract void deleteAll();

}
