package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.unimib_schedule.data.model.Faculty;

import java.util.List;

@Dao
public abstract class FacultyDao extends BaseDao<Faculty> {
    @Query("SELECT * FROM Faculty")
    public abstract LiveData<List<Faculty>> findAll();

    @Query("SELECT * FROM Faculty WHERE id = :facultyId")
    public abstract LiveData<Faculty> findById(String facultyId);

    @Query("SELECT * FROM Faculty WHERE code = :facultyCode")
    public abstract LiveData<Faculty> findByCode(String facultyCode);

    @Query("SELECT * FROM Faculty WHERE code = :facultyCode")
    public abstract Faculty findByCodeSync(String facultyCode);

    @Query("DELETE FROM Faculty")
    public abstract void deleteAll();
}
