package com.unimib_schedule.data.source.remote;

import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.DegreeCourse;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.Professor;
import com.unimib_schedule.data.model.ProfessorTeachingCourse;

import java.util.LinkedList;
import java.util.List;

public class DidacticPeriodResult {
    public List<DegreeCourse> degreeCourses;
    public List<Course> courses;
    public List<Professor> professors;
    public List<Lecture> lectures;
    public List<ProfessorTeachingCourse> ptcList;

    public DidacticPeriodResult(List<DegreeCourse> degreeCourses, List<Course> courses,
                                List<Professor> professors, List<Lecture> lectures, List<ProfessorTeachingCourse> ptcList) {
        this.degreeCourses = degreeCourses;
        this.courses = courses;
        this.professors = professors;
        this.lectures = lectures;
        this.ptcList = ptcList;
    }

    public DidacticPeriodResult() {
        this.degreeCourses = new LinkedList<>();
        this.courses = new LinkedList<>();
        this.lectures = new LinkedList<>();
        this.professors = new LinkedList<>();
        this.ptcList = new LinkedList<>();
    }
}
