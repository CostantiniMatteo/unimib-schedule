package com.unimib_schedule.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.unimib_schedule.data.model.ProfessorTeachingCourse;

import java.util.List;

@Dao
public abstract class ProfessorTeachingCourseDao extends BaseDao<ProfessorTeachingCourse> {
    @Query("SELECT * FROM ProfessorTeachingCourse")
    public abstract LiveData<List<ProfessorTeachingCourse>> findAll();

    @Query("SELECT * FROM ProfessorTeachingCourse WHERE courseId = :courseId")
    public abstract LiveData<List<ProfessorTeachingCourse>> findByCourse(String courseId);

    @Query("SELECT * FROM ProfessorTeachingCourse WHERE professorEmail = :professorEmail")
    public abstract LiveData<List<ProfessorTeachingCourse>> findByProfessor(String professorEmail);

    @Query("DELETE FROM ProfessorTeachingCourse")
    public abstract void deleteAll();

}
