package com.unimib_schedule.data.source.remote;

import com.unimib_schedule.data.model.AcademicYear;
import com.unimib_schedule.data.model.DidacticPeriod;
import com.unimib_schedule.data.model.Faculty;

import java.util.LinkedList;
import java.util.List;

public class UniversityResult {
    public List<Faculty> faculties;
    public List<AcademicYear> academicYears;
    public List<DidacticPeriod> didacticPeriods;

    public UniversityResult(List<Faculty> faculties, List<AcademicYear> academicYears, List<DidacticPeriod> didacticPeriods) {
        this.faculties = faculties;
        this.academicYears = academicYears;
        this.didacticPeriods = didacticPeriods;
    }

    public UniversityResult() {
        this.faculties = new LinkedList<>();
        this.academicYears = new LinkedList<>();
        this.didacticPeriods = new LinkedList<>();
    }
}
