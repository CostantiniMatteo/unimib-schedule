package com.unimib_schedule.display_lectures;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.utils.BaseViewModel;

import java.util.List;

public class DisplayLecturesViewModel extends BaseViewModel {

    public DisplayLecturesViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<List<Lecture>> getLecturesByCourseIds(List<String> courseIds) {
        return repository.getLecturesByCourseIds(courseIds);
    }

    public LiveData<List<Course>> getCoursesByCourseIds(List<String> courseIds) {
        return repository.getCoursesByCourseIds(courseIds);
    }

    public void deletePersonaleSchedule() {
        repository.deletePersonalSchedule();
    }

    public LiveData<List<PersonalScheduleCourse>> getPersonalSchedule() {
        return repository.getPersonalSchedule();
    }
}
