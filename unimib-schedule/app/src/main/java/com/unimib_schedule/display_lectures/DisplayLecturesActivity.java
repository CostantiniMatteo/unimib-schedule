package com.unimib_schedule.display_lectures;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.unimib_schedule.R;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/***
 * Display the calendar with all the lectures associated to the Courses whose ISs are passed
 * with the {COURSE_IDS_KEY} argument. Launching this activity also allows to save the calendar.
 */
public class DisplayLecturesActivity extends BaseActivity implements DisplayLecturesFragment.OnFragmentInteractionListener {
    public static final String COURSE_IDS_KEY = "com.unimib_schedule.display_lectures.course_ids";

    private DisplayLecturesViewModel viewModel;
    private ArrayList<Lecture> retrievedLectures;
    private ArrayList<Course> retrievedCourses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        ArrayList<String> courseIds = getIntent().getStringArrayListExtra(COURSE_IDS_KEY);
        Log.d("DISPLAY LECTURE", courseIds.toString());

        viewModel = ViewModelProviders.of(this).get(DisplayLecturesViewModel.class);
        LiveData<List<Lecture>> lecturesLiveData = viewModel.getLecturesByCourseIds(courseIds);
        lecturesLiveData.observe(this, createLecturesObserver());

        LiveData<List<Course>> courseLiveData = viewModel.getCoursesByCourseIds(courseIds);
        courseLiveData.observe(this, createCoursesObserver());
    }

    private Observer<List<Lecture>> createLecturesObserver() {
        return new Observer<List<Lecture>>() {
            @Override
            public void onChanged(List<Lecture> lectures) {
                retrievedLectures = new ArrayList<>(lectures);
                if (retrievedCourses != null) {
                    launchFragment();
                }
            }
        };
    }

    private Observer<List<Course>> createCoursesObserver() {
        return new Observer<List<Course>>() {
            @Override
            public void onChanged(List<Course> courses) {
                retrievedCourses = new ArrayList<>(courses);

                if (retrievedLectures != null) {
                    launchFragment();
                }
            }
        };
    }

    private void launchFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        DisplayLecturesFragment fragment = new DisplayLecturesFragment();

        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList(DisplayLecturesFragment.LECTURES_KEY, retrievedLectures);
        arguments.putParcelableArrayList(DisplayLecturesFragment.COURSES_KEY, retrievedCourses);
        arguments.putBoolean(DisplayLecturesFragment.SAVE_FAB_ENABLED, true);
        fragment.setArguments(arguments);

        ft.replace(R.id.fragment, fragment);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}
