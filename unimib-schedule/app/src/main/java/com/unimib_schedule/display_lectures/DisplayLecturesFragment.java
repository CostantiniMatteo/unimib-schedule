package com.unimib_schedule.display_lectures;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.unimib_schedule.R;
import com.unimib_schedule.data.model.Course;
import com.unimib_schedule.data.model.Lecture;
import com.unimib_schedule.data.model.PersonalScheduleCourse;
import com.unimib_schedule.edit_schedule.EditScheduleActivity;
import com.unimib_schedule.home.HomeActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * Fragment containing a WeekView that shows a calendar with all the lectures.
 * Courses and Lectures must be passed as arguments using {COURSES_KEY} and {LECTURES_KEY}.
 * Use {EDIT_FAB_ENABLED}, {SAVE_FAB_ENABLED} and {DELETE_ITEM_ENABLED} to customize the view.
 */
public class DisplayLecturesFragment extends Fragment implements MonthLoader.MonthChangeListener, WeekView.EventClickListener {
    public static final String COURSES_KEY = "com.unimib_schedule.display_lectures.courses_key";
    public static final String LECTURES_KEY = "com.unimib_schedule.display_lectures.lectures_key";
    public static final String EDIT_FAB_ENABLED = "com.unimib_schedule.display_lectures.edit_fab_enabled";
    public static final String SAVE_FAB_ENABLED = "com.unimib_schedule.display_lectures.save_fab_enabled";
    public static final String DELETE_ITEM_ENABLED = "com.unimib_schedule.display_lectures.delete_item_enabled";

    private Menu menu;
    private FloatingActionButton saveFab;
    private FloatingActionButton editFab;
    private OnFragmentInteractionListener mListener;
    private List<Lecture> retrievedLectures;
    private List<Course> retrievedCourses;
    private Map<Course, Integer> colorMap;

    private boolean isDeleteMenuItemEnabled;
    private boolean isEditFabEnabled;
    private boolean isSaveFabEnabled;

    private WeekView mWeekView;
    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_THREE_DAY_VIEW = 2;
    private static final int TYPE_FIVE_DAY_VIEW = 3;
    private int mWeekViewType = TYPE_FIVE_DAY_VIEW;
    private int tooltipOffset;

    private DisplayLecturesViewModel viewModel;


    public DisplayLecturesFragment() {
    }

    /***
     *
     * @param enabled if true shows the fab used to save a schedule and makes it interactive.
     *                if false hides the fab.
     */
    public void setSaveFabEnabled(boolean enabled) {
        if (saveFab == null) {
            return;
        }

        saveFab.setClickable(enabled);
        if (enabled) {
            saveFab.show();
        } else {
            saveFab.hide();
        }
    }

    /***
     *
     * @param enabled if true shows the fab used to edit a schedule and makes it interactive.
     *                if false hides the fab.
     */
    public void setEditFabEnabled(boolean enabled) {
        if (editFab == null) {
            return;
        }

        editFab.setClickable(enabled);
        if (enabled) {
            editFab.show();
        } else {
            editFab.hide();
        }
    }

    /***
     *
     * @param enabled if true shows the option to delete the schedule in the toolbar.
     *                if false hides the option.
     */
    public void setDeleteItemEnabled(boolean enabled) {
        if (menu != null) {
            MenuItem deleteScheduleItem = menu.findItem(R.id.action_delete);
            deleteScheduleItem.setVisible(enabled);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_display_lectures, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle arguments = getArguments();

        // TODO: Prima c'era this invece di getActivity()
        viewModel = ViewModelProviders.of(getActivity()).get(DisplayLecturesViewModel.class);

        // Set up buttons and items
        isDeleteMenuItemEnabled = arguments.getBoolean(DELETE_ITEM_ENABLED, false);
        isEditFabEnabled = arguments.getBoolean(EDIT_FAB_ENABLED, false);
        isSaveFabEnabled = arguments.getBoolean(SAVE_FAB_ENABLED, false);

        saveFab = view.findViewById(R.id.saveFab);
        saveFab.setOnClickListener(createSaveFabClickListener());

        editFab = view.findViewById(R.id.editFab);
        editFab.setOnClickListener(createEditFabClickListener());

        setSaveFabEnabled(isSaveFabEnabled);
        setEditFabEnabled(isEditFabEnabled);
        setDeleteItemEnabled(isDeleteMenuItemEnabled);

        // Setup ColorMap
        retrievedLectures = getArguments().getParcelableArrayList(LECTURES_KEY);
        retrievedCourses = getArguments().getParcelableArrayList(COURSES_KEY);

        this.colorMap = new HashMap<Course, Integer>();
        int[] colors = getContext().getResources().getIntArray(R.array.week_view_colors);
        int i = 0;
        for (Course c : retrievedCourses) {
            colorMap.put(c, colors[i]);
            i = (i + 1) % colors.length;
        }

        // Setup WeekView
        mWeekView = view.findViewById(R.id.weekView);
        mWeekView.setMonthChangeListener(this);
        setFiveDayView(Calendar.getInstance());

        mWeekView.setEventClickListener(this);

        // Setup tooltip "no lectures";
        // ^ implemented but not used
        int screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        int fragmentHeight = screenHeight - actionBarHeight;
        tooltipOffset = (int) (-fragmentHeight * 0.30);
    }


    private View.OnClickListener createEditFabClickListener() {
        final Fragment fragment = this;
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editFab.setClickable(false);
                LiveData<List<PersonalScheduleCourse>> personalScheduleCourseLiveData = viewModel.getPersonalSchedule();
                personalScheduleCourseLiveData.observe(fragment, new Observer<List<PersonalScheduleCourse>>() {
                    @Override
                    public void onChanged(List<PersonalScheduleCourse> personalScheduleCourses) {
                        Intent intent = new Intent(getActivity(), EditScheduleActivity.class);
                        intent.putParcelableArrayListExtra(EditScheduleActivity.PERSONAL_SCHEDULE_KEY, new ArrayList<>(personalScheduleCourses));
                        startActivity(intent);
                    }
                });
            }
        };
    }

    @NotNull
    private View.OnClickListener createSaveFabClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.confirmation_dialog_title))
                        .setMessage(getString(R.string.save_schedule_confirmation_text))
                        .setIcon(R.drawable.ic_warning_yellow_24dp)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                saveFab.setClickable(false);
                                ArrayList<PersonalScheduleCourse> personalScheduleCourses = new ArrayList<>(retrievedCourses.size());
                                for (Course c : retrievedCourses) {
                                    personalScheduleCourses.add(new PersonalScheduleCourse(c, true));
                                }

                                Intent intent = new Intent(getActivity(), EditScheduleActivity.class);
                                intent.putParcelableArrayListExtra(EditScheduleActivity.PERSONAL_SCHEDULE_KEY, personalScheduleCourses);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();

            }
        };
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.display_lectures_menu, menu);

        this.menu = menu;

        MenuItem deleteScheduleItem = menu.findItem(R.id.action_delete);
        deleteScheduleItem.setVisible(isDeleteMenuItemEnabled);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Calendar previousDate = mWeekView.getFirstVisibleDay();
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_today:
                mWeekView.goToToday();
                return true;

            case R.id.action_delete:
                new AlertDialog.Builder(getContext())
                        .setTitle(getString(R.string.confirmation_dialog_title))
                        .setMessage(getString(R.string.delete_schedule_confirmation_text))
                        .setIcon(R.drawable.ic_warning_yellow_24dp)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        viewModel.deletePersonaleSchedule();
                                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                                | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                        getActivity().overridePendingTransition(0, 0);
                                        getActivity().finish();

                                        getActivity().overridePendingTransition(0, 0);
                                        startActivity(intent);
                                    }
                                }).start();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
                return true;

            case R.id.display_menu_one_day:
                if (mWeekViewType != TYPE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    setSingleDayView(previousDate);
                }
                return true;

            case R.id.display_menu_three_days:
                if (mWeekViewType != TYPE_THREE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    setThreeDayView(previousDate);
                }
                return true;

            case R.id.display_menu_five_days:
                if (mWeekViewType != TYPE_FIVE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    setFiveDayView(previousDate);
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

    private void setFiveDayView(Calendar previousDate) {
        mWeekViewType = TYPE_FIVE_DAY_VIEW;
        mWeekView.setNumberOfVisibleDays(5);

        // Lets change some dimensions to best fit the view.
        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
        mWeekView.goToDate(previousDate);
    }

    private void setThreeDayView(Calendar previousDate) {
        mWeekViewType = TYPE_THREE_DAY_VIEW;
        mWeekView.setNumberOfVisibleDays(3);

        // Lets change some dimensions to best fit the view.
        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.goToDate(previousDate);
    }

    private void setSingleDayView(Calendar previousDate) {
        mWeekViewType = TYPE_DAY_VIEW;
        mWeekView.setNumberOfVisibleDays(1);

        // Lets change some dimensions to best fit the view.
        mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
        mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
        mWeekView.goToDate(previousDate);
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        saveFab.setClickable(true);
    }

    @Nullable
    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<Lecture> lectures = getLecturesForYearAndMonth(newYear, newMonth);
        List<WeekViewEvent> result = new ArrayList<>(lectures.size());

        Log.d("DISPLAY_LECTURE", "onMonthChanged(" + newYear + ", " + newMonth + ")");
        Log.d("DISPLAY_LECTURE", "retrievedLectures: " + retrievedLectures.toString());
        Log.d("DISPLAY_LECTURE", "filteredLectures: " + lectures.toString());

        for (Lecture lecture : lectures) {
            Course course = getLectureCourse(lecture);
            String location = lecture.building + ", " + lecture.room;
            String lectureId = lecture.room + " " + lecture.beginDate + "__" + lecture.endDate;

            Calendar startTime = (Calendar) Calendar.getInstance().clone();
            startTime.setTime(lecture.beginDate);
            Calendar endTime = (Calendar) Calendar.getInstance().clone();
            endTime.setTime(lecture.endDate);
            WeekViewEvent newLecture = new WeekViewEvent(lectureId, course.name, location, startTime, endTime, false);

            newLecture.setColor(colorMap.get(course));
            result.add(newLecture);
        }

        return result;
    }

    private List<Lecture> getLecturesForYearAndMonth(int year, int month) {

        Calendar calendar = Calendar.getInstance();
        calendar.clear();

        calendar.set(year, month - 1, 1, 0, 0);
        Date startDate = calendar.getTime();

        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(year, month - 1, daysInMonth, 23, 59, 59);
        Date endDate = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        List<Lecture> result = new ArrayList<>();
        for (Lecture l : retrievedLectures) {
            if (startDate.before(l.beginDate) && endDate.after(l.endDate)) {
                Log.d("DISPLAY_LECTURES", "Course: " + l.course +
                        "; Begin: " + dateFormat.format(l.beginDate) +
                        "; End: " + dateFormat.format(l.endDate));
                result.add(l);
            }
        }

        return result;
    }

    private Course getLectureCourse(Lecture lecture) {
        for (Course c : retrievedCourses) {
            if (c.id.equals(lecture.course)) {
                return c;
            }
        }

        return null;
    }

    @Override
    public void onEventClick(@NotNull WeekViewEvent weekViewEvent, @NotNull RectF rectF) {
        new AlertDialog.Builder(getContext())
                .setTitle("Dettaglio Lezione")
                .setMessage(Html.fromHtml("<b>Nome: </b>") + weekViewEvent.getName() + "\n" +
                        Html.fromHtml("<b>Inizio: </b>") + getFormattedDate(weekViewEvent.getStartTime()) + "\n" +
                        Html.fromHtml("<b>Fine: </b>") + getFormattedDate(weekViewEvent.getEndTime()) + "\n" +
                        Html.fromHtml("<b>Aula: </b>") + weekViewEvent.getLocation())
                .setIcon(R.drawable.ic_info_outline_blue_24dp)
                .setNegativeButton(android.R.string.yes, null).show();
    }

    private String getFormattedDate(Calendar calendar) {
        return calendar.get(Calendar.DAY_OF_MONTH) + "-" +
                calendar.get(Calendar.MONTH) + "-" +
                calendar.get(Calendar.YEAR) + " alle " +
                calendar.get(Calendar.HOUR_OF_DAY) + ":" +
                calendar.get(Calendar.MINUTE);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
