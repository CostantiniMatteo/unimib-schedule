\section{Descrizione Generale}\label{sec:descrizione-generale}

\subsection{Prospetto sul Prodotto}
Unimib Schedule è un'applicazione che permette di consultare e salvare sul proprio smartphone l'orario delle lezioni dei corsi dell'Università degli Studi di Milano-Bicocca.

\begin{figure}[h]
  \centering
  \includesvg[width=0.8\textwidth]{res/prospect.svg}
  \caption{Prospetto del prodotto}
\end{figure}

L'applicazione è stata pensata per poter essere utilizzata sia dagli studenti che dai professori universitari; i primi in quanto interessati a una visualizzazione orientata al proprio corso di laurea, con l'aggiunta di eventuali corsi non afferenti al proprio corso di studi, mentre i secondi poiché interessati a visualizzare l'orario delle lezioni che dovranno tenere.

Unimib Schedule nasce dall'esigenza di rendere più accessibile la consultazione degli orari dell'università.
Al momento le due alternative disponibili sono la visualizzazione tramite la nuova piattaforma web che, però, al momento non è utilizzabile da dispositivi mobili, e l'utilizzo dell'app Unimib Course disponibile per Android e iOS, che presenta un'interfaccia poco user-friendly e che permette soltanto la visualizzazione dell'orario di una singola giornata.

L'applicazione si pone l'obiettivo di andare a migliorare le seguenti criticità riscontrate nell'utilizzo di Unimib Course (oltre che a integrare nuove funzionalità):
\begin{itemize}
    \item la veste grafica non è particolarmente accattivante né moderna
    \item la visualizzazione non è ottimizzata per schermi con notch
    \item dalla schermata home non è chiaro come aggiungere un orario dei corsi
    \item la dicitura \emph{profili}, che permetterebbe di aggiungere un orario, non è di facile comprensione
    \item il form per la scelta del profilo non rispetta le metafore classiche della UI mobile
    \item alcune schermate sono visualizzate con del testo fuori dai margini
    \item non è chiaro che tipo di messaggi siano visualizzati nella sezione \emph{messaggi}
\end{itemize}

In modo da essere utilizzabile dalla maggior parte degli studenti della Bicocca l'applicazione dovrà supportare i sistemi operativi Android ed iOS rispettivamente dalla versione 5.1 Lollipop e da iOS 10.
Inoltre l'applicazione dovrà integrarsi con il sistema di notifiche push di entrambi i sistemi operativi, con i rispettivi calendari e con le API disponibili per ottenere le informazioni relative agli orari delle lezioni.


\subsection{Dati e Funzioni del Software}

Nella sezione 2.1 abbiamo evidenziato le criticità dell'applicazione Unimib Course, quindi parlando delle funzionalità dell'applicazione Unimib Schedule abbiamo considerato sia delle migliorie da apportare a Course sia delle piccole aggiunte di funzionalità, arrivando a comporre il seguente elenco:
\begin{itemize}
    \item Aggiornamento della veste grafica e supporto agli schermi con notch
    \item Selezione di un orario preferito
    \item Visualizzazione dell'orario settimanale e non giornaliero, utilizzando la metafora del calendario
    \item Ricerca e visualizzazione orari riguardanti corsi di studio e professori
    \item Composizione personalizzata del proprio orario
    \item Esportazione dell'orario sul calendario del dispositivo
    \item Invio di notifiche per avvisare dell'inizio delle lezioni o di modifiche all'orario
\end{itemize}

\pagebreak
\begin{figure}[h!]
  \centering
  \includesvg[width=1.0\textwidth]{res/domain_model.svg}
  \caption{Modello di dominio}
\end{figure}

\subsection{Caratteristiche degli Utenti e Scenari di Utilizzo}
In questa sezione vengono presentati gli utenti interessati al sistema e alcuni possibili scenari di utilizzo comune che possono coinvolgere l'applicazione.

\subsubsection{Utenti del sistema}
L'app Unimib Schedule si rivolge, come già detto, a due diverse tipologie di utenti: gli studenti e i professori universitari.

Entrambe le categorie di utenti si suppone abbiano una certa dimestichezza con l'utilizzo di smartphone e applicazioni mobile e con le classiche metafore di interfaccia grafica. 
Ci aspettiamo che il lessico utilizzato dai professori possa essere diverso da quello usato dagli studenti, e potrebbe quindi essere necessario tenerne conto durante il design dell'interfaccia e durante l'elicitazione dei requisiti. \\
Nonostante indicativamente questi due utenti possano fare uso delle medesime funzioni, in quanto in pratica non c'è nulla che uno studente possa fare sull'applicazione che sia precluso a un professore e viceversa, ci aspettiamo che le due tipologie utilizzino in maniera diversa questo strumento:
\begin{itemize}
    \item \textbf{Studente:} interessato a salvare sull'applicazione l'orario del corso di laurea che sta intraprendendo al momento, con eventuali aggiunte in seguito alla selezione di insegnamenti esterni al suo corso di appartenenza, a memorizzare l'orario sull'app di calendario nativa del suo smartphone e a ricevere notifiche che lo avvisino dell'inizio delle lezioni.
    \item \textbf{Professore:} interessato a salvare sull'applicazione l'orario riguardante tutti i corsi in cui figura come insegnante, a memorizzare l'orario sull'app di calendario nativa del suo smartphone e a ricevere notifiche che lo avvisino dell'inizio delle lezioni.
\end{itemize}

\subsubsection{Scenari di Utilizzo}
Trattando l'utilizzo che gli utenti potrebbero fare di Unimib Schedule sono stati individuati i seguenti possibili scenari d'uso dell'applicazione.

\myparagraph{Scenario 1: Selezione di un orario preferito}
Un utente desidera selezionare un orario in particolare da salvare come suo orario preferito all'interno dell'app.
L'utente apre quindi l'app e, tramite una ricerca, può accedere ad un dato corso di laurea di suo interesse o ad un professore in particolare presente in lista. Selezionandoli, l'utente accede quindi alla lista di tutti gli insegnamenti presenti all'interno del corso di laurea o degli insegnamenti tenuti da quel professore e passa alla selezione di quelli che intende frequentare o che, semplicemente, vuole aggiungere al proprio orario personale.
Terminata questa operazione, avrà quindi a disposizione in una sezione dedicata una timetable settimanale contenente le informazioni sui corsi.

\myparagraph{Scenario 2: Aggiunta di un insegnamento all'orario preferito, esterno al corso di laurea relativo}
Un utente desidera aggiungere al suo orario un insegnamento che non rientra in quelli del corso di laurea che ha scelto come predefinito alla prima ricerca effettuata.
L'utente accede quindi alla funzione per modificare l'orario e aggiungere un insegnamento esterno al suo orario, effettuando quindi una ricerca per trovare il corso di laurea nel quale rientra quello di suo interesse. Una volta trovato, l'utente può quindi selezionare l'insegnamento o gli insegnamenti da inserire nella sua timetable personale.

\myparagraph{Scenario 3: Esportazione dell'orario sul calendario personale}
Un utente vuole trasferire le informazioni dell'orario presente nella sua schedule personale all'interno dell'applicazione calendario sul suo dispositivo.
L'utente accede quindi alla sezione relativa all'orario e, tramite la funzione di esportazione, l'applicazione si occupa di trasferire nel calendario del dispositivo la timetable, istanziando un apposito calendario per l'orario universitario.


\subsection{Vincoli}
Unimib Schedule, per come è progettata, deve interfacciarsi anche con elementi esterni quali il calendario nativo dello smartphone su cui è installata, il sistema di notifiche dello smartphone e le API di Unimib per scaricare le informazioni relative all'orario. Per questo motivo è stato necessario porre dei vincoli relativi a queste specifiche interazioni:

\begin{itemize}
    \item \textbf{Integrazione con il calendario}: deve essere possibile esportare l'orario salvato sul calendario ed è quindi necessario integrarsi con le modalità specificate da entrambi i sistemi operativi.
    Deve essere inoltre possibile selezionare il calendario corretto su cui esportare l'orario senza andare a modificare calendari pre-esistenti.
    
    \item \textbf{Integrazione con il sistema di notifiche push}: l'applicazione deve essere in grado di inviare notifiche push sia per avvisare l'utilizzatore di una lezione imminente, sia per segnalare nuove modifiche all'orario.
    Per fare questo l'app deve necessariamente appoggiarsi ai sistemi di notifica push dei sistemi operativi Android e iOS ed è quindi necessario che si integri con questi.
    
    \item \textbf{Integrazione con le API Unimib}: per recuperare le informazioni relative alle lezioni è necessario utilizzare le apposite API.
    Non è ancora specificato quale soluzione utilizzare: una possibile soluzione consiste nelle API utilizzare dal servizio web (\url{http://gestioneorari.didattica.Unimib.it/PortaleStudentiUnimib}).
    Alternativamente, l'applicazione Unimib Course potrebbe utilizzare API differenti, ma deve essere ancora verificato.
\end{itemize}
